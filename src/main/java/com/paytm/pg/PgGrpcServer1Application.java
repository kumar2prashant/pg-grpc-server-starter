package com.paytm.pg;


import com.paytm.pg.server.infrastructure.GracefulShutdown;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.time.Clock;
import java.util.Date;
import java.util.TimeZone;

@Slf4j
@EnableRetry
@SpringBootApplication
@EnableAutoConfiguration
@EnableTransactionManagement(proxyTargetClass = true) 
@MapperScan(basePackages = "com.paytm.pg.db.mapper")
public class PgGrpcServer1Application {

	@Value("${graceful-shutdown.tomcat.keep-alive-timeout-in-seconds}")
	private int keepAliveTimeoutInSeconds;
	
	@Value("${component.settings.timezone}")
    private String timezone;

	@Bean
	public GracefulShutdown gracefulShutdown(ApplicationContext context) {
		return new GracefulShutdown(context);
	}

	@Bean
	public WebServerFactory servletContainerFactory(final GracefulShutdown gracefulShutdown) {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		// add keep alive timeout for tomcat
		// server will cut off connection actively when a tcp connection is idle for such period of time
		// note some http client can handle this properly but some can not, so as back up plan, we also add keep-alive header
		factory.addConnectorCustomizers(connector ->
				((AbstractHttp11Protocol) connector.getProtocolHandler()).setKeepAliveTimeout(keepAliveTimeoutInSeconds * 1000));
		// adds graceful shutdown
		factory.addConnectorCustomizers(gracefulShutdown);
		return factory;
	}
	
	@PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone(timezone));
        log.info("Spring boot application running with UTC timezone: {}", new Date());
    }
	
	@Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

	public static void main(String[] args) {
		SpringApplication.run(PgGrpcServer1Application.class, args);
	}
}
