package com.paytm.pg.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.paytm.pg.db.bean.Order;
import com.paytm.pg.db.mapper.OrderMapper;
import com.paytm.pg.server.producer.KafkaProducer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceDB {

    @Autowired
    private OrderMapper orderMapper;
    
    @Autowired
    KafkaProducer kafkaProducer;
    
    @Value("${redis.expireDurationSec}")
    private long expireDurationSec;

    private final RedisTemplate<String, String> redisTemplate;

    @Transactional(propagation = Propagation.REQUIRED , rollbackFor = Exception.class)
    public void save(Order order){
    	String key = Long.toString(order.getOrderId()); 
    	boolean result = false;
        try {
            result = (Boolean) redisTemplate.execute((RedisCallback<Boolean>) connection -> {
                if (connection.exists(key.getBytes())) {
                    return true;
                }
                connection.setEx(key.getBytes(), expireDurationSec, key.getBytes());
                return false;
            });
        } catch (Exception e) {
            log.info("checking similar order failed, because of Redis connection failure", e);
        }

        redisTemplate.execute((RedisCallback<Boolean>) connection -> {
            if (connection.exists(key.getBytes())) {
                log.info("************* Exists in Redis *************");
                return true;
            }
            connection.setEx(key.getBytes(), expireDurationSec, key.getBytes());
            log.info("************* Save in Redis *************");
            return false;
        });

        if(result) {
        	log.info("Orderalready Exists");
        }else {
        	orderMapper.insertOrder(order);
        	kafkaProducer.publishMessage("pg-test", Long.toString(order.getUserId()), order.toString());
        	int x = 0;
        	int y = 1;
        	
        	//y = y/x;
        	
        	//orderMapper.insertOrder(order);
        }
    }

    public Order findById(Long userId, Long orderId){
        return orderMapper.findById(userId, orderId);
    }
    
    public Order findByUserId(Long userId){
        return orderMapper.findByUserId(userId);
    }
    
    public Order findByOrderId(Long orderId){
        return orderMapper.findByOrderId(orderId);
    }

}
