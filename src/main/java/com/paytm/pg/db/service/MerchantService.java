package com.paytm.pg.db.service;

import com.google.common.base.Verify;
import com.google.common.base.VerifyException;
import com.google.common.util.concurrent.Uninterruptibles;
import com.google.rpc.DebugInfo;
import com.paytm.pg.Merchant;
import com.paytm.pg.MerchantServiceGrpc.MerchantServiceBlockingStub;
import com.paytm.pg.MerchantServiceGrpc.MerchantServiceStub;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class MerchantService {

	  @GrpcClient("local-merchant-service")
	  private MerchantServiceBlockingStub blockingStub;
	  
	  @GrpcClient("local-merchant-service")
	  private MerchantServiceStub asyncStub;
	  
	  private static final Metadata.Key<DebugInfo> DEBUG_INFO_TRAILER_KEY =
		      ProtoUtils.keyForProto(DebugInfo.getDefaultInstance());

      private static final DebugInfo DEBUG_INFO =
		      DebugInfo.newBuilder()
		          .addStackEntries("stack_entry_1")
		          .addStackEntries("stack_entry_2")
		          .addStackEntries("stack_entry_3")
		          .setDetail("detailed error info.").build();

		  private static final String DEBUG_DESC = "detailed error description";

	  /**
	   * Blocking unary call example.  saving merchant .
	   */

	  public void saveMerchant() {
		    log.info("Saving merchant");

		    Merchant merchant;
		    try {
		    	merchant = blockingStub
						.withDeadlineAfter(10, TimeUnit.MILLISECONDS)
						.saveMerchant(Merchant.newBuilder().setMid(1).build());
		    } catch (StatusRuntimeException e) {
		      warning("RPC failed: {0}", e.getStatus());
		      verifyErrorReply(e);
		      return;
		    }
		    log.info("Save merchant " + merchant.toString());
	  }
	  

	  /**
	   * Blocking unary call example.  Calls findByMerchantId and prints the response.
	   */
	  
	  
	  @Retryable(include = {RuntimeException.class},
	            exclude = {NullPointerException.class},
	            maxAttemptsExpression = "3",
	            backoff = @Backoff(delayExpression = "30", multiplierExpression = "1"))
	  
	  @SneakyThrows
	  public com.paytm.pg.db.bean.Merchant findByMerchantId(final com.paytm.pg.db.bean.Merchant merchantBo) {
	    info("Merchant Request", merchantBo.toString());
	    Merchant merchant = Merchant.newBuilder()
				.setMid(merchantBo.getMid())
				.setContractId(merchantBo.getContractId()).build();
	    
	    try {
	    	merchant = blockingStub
					// .withDeadlineAfter(200, TimeUnit.MILLISECONDS)
					.findByMerchantId(merchant);
	    } catch (StatusRuntimeException e) {
	      warning("RPC failed:"+ e.getStatus());
	      throw new RuntimeException(
                  "Don't worry!! Just Simulated for Spring-retry..Must fallback as all retry will get exception!!!");
     
	    } catch (Throwable t){
			Status status = Status.fromThrowable(t);
			log.error(status.getDescription());
			log.error(status.toString());
		}
	    
	    return com.paytm.pg.db.bean.Merchant.fromProto(merchant);
	  }
	  
	  /**
	     * Recover from timed out alipay payment, subsequent arguments are populated
	     * from the argument list of the failed method in order.
	     */
	    @Recover
	    public String recover(RuntimeException exception) {
	        if (exception instanceof StatusRuntimeException) {
	            throw new RuntimeException("Grpc System Error");
	        }
	        if (exception instanceof RuntimeException) {
	            throw exception;
	        }
	        throw new RuntimeException("System Error");
	    }


	  /**
	   * Bi-directional example, which can only be asynchronous. Send some chat messages, and print any
	   * chat messages that are sent from the server.
	   */
	  public com.paytm.pg.db.bean.Merchant findByMerchantIdAsync(com.paytm.pg.db.bean.Merchant merchantBo) throws InterruptedException {
	    info("find By Contract Id Async API");
	    final CountDownLatch finishLatch = new CountDownLatch(1);
	    final List<com.paytm.pg.db.bean.Merchant> merchantResponse = new ArrayList<>();
	    
	    StreamObserver<Merchant> responseObserver =  new StreamObserver<Merchant>() {
	          @Override
	          public void onNext(Merchant note) {
	          	final com.paytm.pg.db.bean.Merchant merchant = com.paytm.pg.db.bean.Merchant.fromProto(note);

	        	  log.info("merchant received |" + merchant.toString());
	        	  merchantResponse.add(merchant);
	          }

	          @Override
	          public void onError(Throwable t) {
	            warning("RouteChat Failed|"+ Status.fromThrowable(t));
	            finishLatch.countDown();
	            
	            Status status = Status.fromThrowable(t);
	            log.error(status.getDescription());
	            log.error(status.toString());
	          }

	          @Override
	          public void onCompleted() {
	            info("Finished Fetching Merchant");
	            finishLatch.countDown();
	          }
	        };
	        
	        StreamObserver<Merchant> requestObserver = asyncStub
					//.withDeadlineAfter(200, TimeUnit.MILLISECONDS)
					.findByMerchantIdAsync(responseObserver);

	    try {
	    	Merchant merchantRequest = Merchant.newBuilder()
					.setMid(merchantBo.getMid())
					.setContractId(merchantBo.getContractId()).build();
	    	requestObserver.onNext(merchantRequest);
	    	
	    } catch (RuntimeException e) {
	      // Cancel RPC
	      requestObserver.onError(e);
	      throw e;
	    }
	    // Mark the end of requests
	    requestObserver.onCompleted();
	    
	    if (!Uninterruptibles.awaitUninterruptibly(finishLatch, 1, TimeUnit.SECONDS)) {
	        throw new RuntimeException("timeout!");
	      }
	    return merchantResponse.get(0);
	  }

	  /** Issues several different requests and then exits. */

	  private void info(String msg, Object... params) {
	    log.info(msg, params);
	  }

	  public void warning(String msg, Object... params) {
		  log.warn(msg, params);
	  }
	  
	  static void verifyErrorReply(Throwable t) {
		    Status status = Status.fromThrowable(t);
		    Metadata trailers = Status.trailersFromThrowable(t);
		    Verify.verify(status.getCode() == Status.Code.INTERNAL);
		    Verify.verify(trailers.containsKey(DEBUG_INFO_TRAILER_KEY));
		    Verify.verify(status.getDescription().equals(DEBUG_DESC));
		    try {
		      Verify.verify(trailers.get(DEBUG_INFO_TRAILER_KEY).equals(DEBUG_INFO));
		    } catch (IllegalArgumentException e) {
		      throw new VerifyException(e);
		    }
		  }
}

