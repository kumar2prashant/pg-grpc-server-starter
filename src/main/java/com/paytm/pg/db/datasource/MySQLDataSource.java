package com.paytm.pg.db.datasource;

import com.zaxxer.hikari.HikariDataSource;

import lombok.Data;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
public class MySQLDataSource extends HikariDataSource {
    private Map<String, String> config;

    public void addConfig(String key, String value) {
        if (config==null){
            config = new HashMap<>();
        }
        config.put(key, value);

    }
}
