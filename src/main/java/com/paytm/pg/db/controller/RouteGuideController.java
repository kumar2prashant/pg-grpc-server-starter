package com.paytm.pg.db.controller;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paytm.pg.db.bean.Root;
import com.paytm.pg.db.service.RouteGuideUtil;
import io.grpc.examples.routeguide.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.lang.Math.*;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * A sample gRPC server that serve the RouteGuide (see route_guide.proto) service.
 */
@Slf4j
@GrpcService
public class RouteGuideController extends RouteGuideGrpc.RouteGuideImplBase {
	
	private Gson gson = new Gson();
	private static final String FEATURES = "{\"feature\":[{\"location\":{\"latitude\":407838351,\"longitude\":-746143763},\"name\":\"Patriots Path, Mendham, NJ 07945, USA\"},{\"location\":{\"latitude\":408122808,\"longitude\":-743999179},\"name\":\"101 New Jersey 10, Whippany, NJ 07981, USA\"},{\"location\":{\"latitude\":413628156,\"longitude\":-749015468},\"name\":\"U.S. 6, Shohola, PA 18458, USA\"},{\"location\":{\"latitude\":419999544,\"longitude\":-740371136},\"name\":\"5 Conners Road, Kingston, NY 12401, USA\"},{\"location\":{\"latitude\":414008389,\"longitude\":-743951297},\"name\":\"Mid Hudson Psychiatric Center, New Hampton, NY 10958, USA\"},{\"location\":{\"latitude\":419611318,\"longitude\":-746524769},\"name\":\"287 Flugertown Road, Livingston Manor, NY 12758, USA\"},{\"location\":{\"latitude\":406109563,\"longitude\":-742186778},\"name\":\"4001 Tremley Point Road, Linden, NJ 07036, USA\"},{\"location\":{\"latitude\":416802456,\"longitude\":-742370183},\"name\":\"352 South Mountain Road, Wallkill, NY 12589, USA\"},{\"location\":{\"latitude\":412950425,\"longitude\":-741077389},\"name\":\"Bailey Turn Road, Harriman, NY 10926, USA\"},{\"location\":{\"latitude\":412144655,\"longitude\":-743949739},\"name\":\"193-199 Wawayanda Road, Hewitt, NJ 07421, USA\"},{\"location\":{\"latitude\":415736605,\"longitude\":-742847522},\"name\":\"406-496 Ward Avenue, Pine Bush, NY 12566, USA\"},{\"location\":{\"latitude\":413843930,\"longitude\":-740501726},\"name\":\"162 Merrill Road, Highland Mills, NY 10930, USA\"},{\"location\":{\"latitude\":410873075,\"longitude\":-744459023},\"name\":\"Clinton Road, West Milford, NJ 07480, USA\"},{\"location\":{\"latitude\":412346009,\"longitude\":-744026814},\"name\":\"16 Old Brook Lane, Warwick, NY 10990, USA\"},{\"location\":{\"latitude\":402948455,\"longitude\":-747903913},\"name\":\"3 Drake Lane, Pennington, NJ 08534, USA\"},{\"location\":{\"latitude\":406337092,\"longitude\":-740122226},\"name\":\"6324 8th Avenue, Brooklyn, NY 11220, USA\"},{\"location\":{\"latitude\":406421967,\"longitude\":-747727624},\"name\":\"1 Merck Access Road, Whitehouse Station, NJ 08889, USA\"},{\"location\":{\"latitude\":416318082,\"longitude\":-749677716},\"name\":\"78-98 Schalck Road, Narrowsburg, NY 12764, USA\"},{\"location\":{\"latitude\":415301720,\"longitude\":-748416257},\"name\":\"282 Lakeview Drive Road, Highland Lake, NY 12743, USA\"},{\"location\":{\"latitude\":402647019,\"longitude\":-747071791},\"name\":\"330 Evelyn Avenue, Hamilton Township, NJ 08619, USA\"},{\"location\":{\"latitude\":412567807,\"longitude\":-741058078},\"name\":\"New York State Reference Route 987E, Southfields, NY 10975, USA\"},{\"location\":{\"latitude\":416855156,\"longitude\":-744420597},\"name\":\"103-271 Tempaloni Road, Ellenville, NY 12428, USA\"},{\"location\":{\"latitude\":404663628,\"longitude\":-744820157},\"name\":\"1300 Airport Road, North Brunswick Township, NJ 08902, USA\"},{\"location\":{\"latitude\":407113723,\"longitude\":-749746483},\"name\":\"\"},{\"location\":{\"latitude\":402133926,\"longitude\":-743613249},\"name\":\"\"},{\"location\":{\"latitude\":400273442,\"longitude\":-741220915},\"name\":\"\"},{\"location\":{\"latitude\":411236786,\"longitude\":-744070769},\"name\":\"\"},{\"location\":{\"latitude\":411633782,\"longitude\":-746784970},\"name\":\"211-225 Plains Road, Augusta, NJ 07822, USA\"},{\"location\":{\"latitude\":415830701,\"longitude\":-742952812},\"name\":\"\"},{\"location\":{\"latitude\":413447164,\"longitude\":-748712898},\"name\":\"165 Pedersen Ridge Road, Milford, PA 18337, USA\"},{\"location\":{\"latitude\":405047245,\"longitude\":-749800722},\"name\":\"100-122 Locktown Road, Frenchtown, NJ 08825, USA\"},{\"location\":{\"latitude\":418858923,\"longitude\":-746156790},\"name\":\"\"},{\"location\":{\"latitude\":417951888,\"longitude\":-748484944},\"name\":\"650-652 Willi Hill Road, Swan Lake, NY 12783, USA\"},{\"location\":{\"latitude\":407033786,\"longitude\":-743977337},\"name\":\"26 East 3rd Street, New Providence, NJ 07974, USA\"},{\"location\":{\"latitude\":417548014,\"longitude\":-740075041},\"name\":\"\"},{\"location\":{\"latitude\":410395868,\"longitude\":-744972325},\"name\":\"\"},{\"location\":{\"latitude\":404615353,\"longitude\":-745129803},\"name\":\"\"},{\"location\":{\"latitude\":406589790,\"longitude\":-743560121},\"name\":\"611 Lawrence Avenue, Westfield, NJ 07090, USA\"},{\"location\":{\"latitude\":414653148,\"longitude\":-740477477},\"name\":\"18 Lannis Avenue, New Windsor, NY 12553, USA\"},{\"location\":{\"latitude\":405957808,\"longitude\":-743255336},\"name\":\"82-104 Amherst Avenue, Colonia, NJ 07067, USA\"},{\"location\":{\"latitude\":411733589,\"longitude\":-741648093},\"name\":\"170 Seven Lakes Drive, Sloatsburg, NY 10974, USA\"},{\"location\":{\"latitude\":412676291,\"longitude\":-742606606},\"name\":\"1270 Lakes Road, Monroe, NY 10950, USA\"},{\"location\":{\"latitude\":409224445,\"longitude\":-748286738},\"name\":\"509-535 Alphano Road, Great Meadows, NJ 07838, USA\"},{\"location\":{\"latitude\":406523420,\"longitude\":-742135517},\"name\":\"652 Garden Street, Elizabeth, NJ 07202, USA\"},{\"location\":{\"latitude\":401827388,\"longitude\":-740294537},\"name\":\"349 Sea Spray Court, Neptune City, NJ 07753, USA\"},{\"location\":{\"latitude\":410564152,\"longitude\":-743685054},\"name\":\"13-17 Stanley Street, West Milford, NJ 07480, USA\"},{\"location\":{\"latitude\":408472324,\"longitude\":-740726046},\"name\":\"47 Industrial Avenue, Teterboro, NJ 07608, USA\"},{\"location\":{\"latitude\":412452168,\"longitude\":-740214052},\"name\":\"5 White Oak Lane, Stony Point, NY 10980, USA\"},{\"location\":{\"latitude\":409146138,\"longitude\":-746188906},\"name\":\"Berkshire Valley Management Area Trail, Jefferson, NJ, USA\"},{\"location\":{\"latitude\":404701380,\"longitude\":-744781745},\"name\":\"1007 Jersey Avenue, New Brunswick, NJ 08901, USA\"},{\"location\":{\"latitude\":409642566,\"longitude\":-746017679},\"name\":\"6 East Emerald Isle Drive, Lake Hopatcong, NJ 07849, USA\"},{\"location\":{\"latitude\":408031728,\"longitude\":-748645385},\"name\":\"1358-1474 New Jersey 57, Port Murray, NJ 07865, USA\"},{\"location\":{\"latitude\":413700272,\"longitude\":-742135189},\"name\":\"367 Prospect Road, Chester, NY 10918, USA\"},{\"location\":{\"latitude\":404310607,\"longitude\":-740282632},\"name\":\"10 Simon Lake Drive, Atlantic Highlands, NJ 07716, USA\"},{\"location\":{\"latitude\":409319800,\"longitude\":-746201391},\"name\":\"11 Ward Street, Mount Arlington, NJ 07856, USA\"},{\"location\":{\"latitude\":406685311,\"longitude\":-742108603},\"name\":\"300-398 Jefferson Avenue, Elizabeth, NJ 07201, USA\"},{\"location\":{\"latitude\":419018117,\"longitude\":-749142781},\"name\":\"43 Dreher Road, Roscoe, NY 12776, USA\"},{\"location\":{\"latitude\":412856162,\"longitude\":-745148837},\"name\":\"Swan Street, Pine Island, NY 10969, USA\"},{\"location\":{\"latitude\":416560744,\"longitude\":-746721964},\"name\":\"66 Pleasantview Avenue, Monticello, NY 12701, USA\"},{\"location\":{\"latitude\":405314270,\"longitude\":-749836354},\"name\":\"\"},{\"location\":{\"latitude\":414219548,\"longitude\":-743327440},\"name\":\"\"},{\"location\":{\"latitude\":415534177,\"longitude\":-742900616},\"name\":\"565 Winding Hills Road, Montgomery, NY 12549, USA\"},{\"location\":{\"latitude\":406898530,\"longitude\":-749127080},\"name\":\"231 Rocky Run Road, Glen Gardner, NJ 08826, USA\"},{\"location\":{\"latitude\":407586880,\"longitude\":-741670168},\"name\":\"100 Mount Pleasant Avenue, Newark, NJ 07104, USA\"},{\"location\":{\"latitude\":400106455,\"longitude\":-742870190},\"name\":\"517-521 Huntington Drive, Manchester Township, NJ 08759, USA\"},{\"location\":{\"latitude\":400066188,\"longitude\":-746793294},\"name\":\"\"},{\"location\":{\"latitude\":418803880,\"longitude\":-744102673},\"name\":\"40 Mountain Road, Napanoch, NY 12458, USA\"},{\"location\":{\"latitude\":414204288,\"longitude\":-747895140},\"name\":\"\"},{\"location\":{\"latitude\":414777405,\"longitude\":-740615601},\"name\":\"\"},{\"location\":{\"latitude\":415464475,\"longitude\":-747175374},\"name\":\"48 North Road, Forestburgh, NY 12777, USA\"},{\"location\":{\"latitude\":404062378,\"longitude\":-746376177},\"name\":\"\"},{\"location\":{\"latitude\":405688272,\"longitude\":-749285130},\"name\":\"\"},{\"location\":{\"latitude\":400342070,\"longitude\":-748788996},\"name\":\"\"},{\"location\":{\"latitude\":401809022,\"longitude\":-744157964},\"name\":\"\"},{\"location\":{\"latitude\":404226644,\"longitude\":-740517141},\"name\":\"9 Thompson Avenue, Leonardo, NJ 07737, USA\"},{\"location\":{\"latitude\":410322033,\"longitude\":-747871659},\"name\":\"\"},{\"location\":{\"latitude\":407100674,\"longitude\":-747742727},\"name\":\"\"},{\"location\":{\"latitude\":418811433,\"longitude\":-741718005},\"name\":\"213 Bush Road, Stone Ridge, NY 12484, USA\"},{\"location\":{\"latitude\":415034302,\"longitude\":-743850945},\"name\":\"\"},{\"location\":{\"latitude\":411349992,\"longitude\":-743694161},\"name\":\"\"},{\"location\":{\"latitude\":404839914,\"longitude\":-744759616},\"name\":\"1-17 Bergen Court, New Brunswick, NJ 08901, USA\"},{\"location\":{\"latitude\":414638017,\"longitude\":-745957854},\"name\":\"35 Oakland Valley Road, Cuddebackville, NY 12729, USA\"},{\"location\":{\"latitude\":412127800,\"longitude\":-740173578},\"name\":\"\"},{\"location\":{\"latitude\":401263460,\"longitude\":-747964303},\"name\":\"\"},{\"location\":{\"latitude\":412843391,\"longitude\":-749086026},\"name\":\"\"},{\"location\":{\"latitude\":418512773,\"longitude\":-743067823},\"name\":\"\"},{\"location\":{\"latitude\":404318328,\"longitude\":-740835638},\"name\":\"42-102 Main Street, Belford, NJ 07718, USA\"},{\"location\":{\"latitude\":419020746,\"longitude\":-741172328},\"name\":\"\"},{\"location\":{\"latitude\":404080723,\"longitude\":-746119569},\"name\":\"\"},{\"location\":{\"latitude\":401012643,\"longitude\":-744035134},\"name\":\"\"},{\"location\":{\"latitude\":404306372,\"longitude\":-741079661},\"name\":\"\"},{\"location\":{\"latitude\":403966326,\"longitude\":-748519297},\"name\":\"\"},{\"location\":{\"latitude\":405002031,\"longitude\":-748407866},\"name\":\"\"},{\"location\":{\"latitude\":409532885,\"longitude\":-742200683},\"name\":\"\"},{\"location\":{\"latitude\":416851321,\"longitude\":-742674555},\"name\":\"\"},{\"location\":{\"latitude\":406411633,\"longitude\":-741722051},\"name\":\"3387 Richmond Terrace, Staten Island, NY 10303, USA\"},{\"location\":{\"latitude\":413069058,\"longitude\":-744597778},\"name\":\"261 Van Sickle Road, Goshen, NY 10924, USA\"},{\"location\":{\"latitude\":418465462,\"longitude\":-746859398},\"name\":\"\"},{\"location\":{\"latitude\":411733222,\"longitude\":-744228360},\"name\":\"\"},{\"location\":{\"latitude\":410248224,\"longitude\":-747127767},\"name\":\"3 Hasta Way, Newton, NJ 07860, USA\"}]}";

    private final Collection<com.paytm.pg.db.bean.Feature> features;
    private final ConcurrentMap<Point, List<RouteNote>> routeNotes =
        new ConcurrentHashMap<Point, List<RouteNote>>();

    public RouteGuideController(Collection<Feature> features) throws JsonSyntaxException, JsonIOException {
    	Root root = gson.fromJson(FEATURES, Root.class);
    	this.features = new ArrayList<com.paytm.pg.db.bean.Feature>(root.getFeature());
    	log.info(root.toString());
    }

    /**
     * Gets the {@link Feature} at the requested {@link Point}. If no feature at that location
     * exists, an unnamed feature is returned at the provided location.
     *
     * @param request the requested location for the feature.
     * @param responseObserver the observer that will receive the feature at the requested point.
     */
    @Override
    public void getFeature(Point request, StreamObserver<Feature> responseObserver) {
      log.info("Entering getFeature");
      responseObserver.onNext(checkFeature(request).toProto());
      responseObserver.onCompleted();
    }

    /**
     * Gets all features contained within the given bounding {@link Rectangle}.
     *
     * @param request the bounding rectangle for the requested features.
     * @param responseObserver the observer that will receive the features.
     */
    @Override
    public void listFeatures(Rectangle request, StreamObserver<Feature> responseObserver) {
    	log.info("Entering listFeatures");
      int left = min(request.getLo().getLongitude(), request.getHi().getLongitude());
      int right = max(request.getLo().getLongitude(), request.getHi().getLongitude());
      int top = max(request.getLo().getLatitude(), request.getHi().getLatitude());
      int bottom = min(request.getLo().getLatitude(), request.getHi().getLatitude());

      for (com.paytm.pg.db.bean.Feature feature : features) {
        if (!RouteGuideUtil.exists(feature)) {
          continue;
        }

        int lat = feature.getLocation().getLatitude();
        int lon = feature.getLocation().getLongitude();
        if (lon >= left && lon <= right && lat >= bottom && lat <= top) {
        	log.info("Found feature|" + feature.toString());
          responseObserver.onNext(feature.toProto());
        }
      }
      responseObserver.onCompleted();
    }

    /**
     * Gets a stream of points, and responds with statistics about the "trip": number of points,
     * number of known features visited, total distance traveled, and total time spent.
     *
     * @param responseObserver an observer to receive the response summary.
     * @return an observer to receive the requested route points.
     */
    @Override
    public StreamObserver<Point> recordRoute(final StreamObserver<RouteSummary> responseObserver) {
      return new StreamObserver<Point>() {
        int pointCount;
        int featureCount;
        int distance;
        Point previous;
        final long startTime = System.nanoTime();

        @Override
        public void onNext(Point point) {
          pointCount++;
          log.info("RecordRoute point" + point.getLongitude() + "|"+ point.getLatitude());

          if (RouteGuideUtil.exists(checkFeature(point))) {
            featureCount++;
          }

          if (previous != null) {
            distance += calcDistance(previous, point);
          }
          previous = point;
        }

        @Override
        public void onError(Throwable t) {
          log.warn("recordRoute cancelled");
        }

        @Override
        public void onCompleted() {
          long seconds = NANOSECONDS.toSeconds(System.nanoTime() - startTime);
          responseObserver.onNext(RouteSummary.newBuilder().setPointCount(pointCount)
              .setFeatureCount(featureCount).setDistance(distance)
              .setElapsedTime((int) seconds).build());
          log.info("RouteSummary|" +pointCount +"|"+featureCount+"|"+distance+"|"+ seconds);
          responseObserver.onCompleted();
        }
      };
    }

    /**
     * Receives a stream of message/location pairs, and responds with a stream of all previous
     * messages at each of those locations.
     *
     * @param responseObserver an observer to receive the stream of previous messages.
     * @return an observer to handle requested message/location pairs.
     */
    @Override
    public StreamObserver<RouteNote> routeChat(final StreamObserver<RouteNote> responseObserver) {
      return new StreamObserver<RouteNote>() {
        @Override
        public void onNext(RouteNote note) {
          List<RouteNote> notes = getOrCreateNotes(note.getLocation());

          // Respond with all previous notes at this location.
//          for (RouteNote prevNote : notes.toArray(new RouteNote[0])) {
//
//          }
          responseObserver.onNext(note);

          // Now add the new note to the list
          notes.add(note);
        }

        @Override
        public void onError(Throwable t) {
          log.warn("routeChat cancelled");
        }

        @Override
        public void onCompleted() {
          responseObserver.onCompleted();
        }
      };
    }

    /**
     * Get the notes list for the given location. If missing, create it.
     */
    private List<RouteNote> getOrCreateNotes(Point location) {
      List<RouteNote> notes = Collections.synchronizedList(new ArrayList<RouteNote>());
      List<RouteNote> prevNotes = routeNotes.putIfAbsent(location, notes);
      return prevNotes != null ? prevNotes : notes;
    }

    /**
     * Gets the feature at the given point.
     *
     * @param location the location to check.
     * @return The feature object at the point. Note that an empty name indicates no feature.
     */
    private com.paytm.pg.db.bean.Feature checkFeature(Point location) {
      log.info("checkFeature|" + location.getLatitude() + "|"+location.getLongitude());
      
      for (com.paytm.pg.db.bean.Feature feature : features) {
        if (feature.getLocation().getLatitude() == location.getLatitude()
            && feature.getLocation().getLongitude() == location.getLongitude()) {
        	
          log.info("Found Feature|" + feature.toString());
          return feature;
        }
      }

      // No feature was found, return an unnamed feature.
      return com.paytm.pg.db.bean.Feature.builder().name("").location(
    		  com.paytm.pg.db.bean.Point.builder().latitude(location.getLatitude()).longitude(location.getLongitude()).build()).build();
    }

    /**
     * Calculate the distance between two points using the "haversine" formula.
     * The formula is based on http://mathforum.org/library/drmath/view/51879.html.
     *
     * @param start The starting point
     * @param end The end point
     * @return The distance between the points in meters
     */
    private static int calcDistance(Point start, Point end) {
      int r = 6371000; // earth radius in meters
      double lat1 = toRadians(RouteGuideUtil.getLatitude(start));
      double lat2 = toRadians(RouteGuideUtil.getLatitude(end));
      double lon1 = toRadians(RouteGuideUtil.getLongitude(start));
      double lon2 = toRadians(RouteGuideUtil.getLongitude(end));
      double deltaLat = lat2 - lat1;
      double deltaLon = lon2 - lon1;

      double a = sin(deltaLat / 2) * sin(deltaLat / 2)
          + cos(lat1) * cos(lat2) * sin(deltaLon / 2) * sin(deltaLon / 2);
      double c = 2 * atan2(sqrt(a), sqrt(1 - a));

      return (int) (r * c);
    }
  
}
