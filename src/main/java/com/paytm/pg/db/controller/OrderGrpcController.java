package com.paytm.pg.db.controller;

import com.paytm.pg.Order;
import com.paytm.pg.OrderServiceGrpc;
import com.paytm.pg.db.service.OrderServiceDB;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@GrpcService
public class OrderGrpcController extends OrderServiceGrpc.OrderServiceImplBase{

    final SnowflakeShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();

    @Autowired
    private OrderServiceDB orderService;

	@Autowired
	private com.paytm.pg.db.processor.OrderProcessor orderProcessor;

    @Override
    public void saveOrder(Order req, StreamObserver<Order> responseObserver) {
        Long id = Long.valueOf(keyGenerator.generateKey().toString());
        com.paytm.pg.db.bean.Order orderPg = com.paytm.pg.db.bean.Order.builder()
                .id(id)
                .orderId(id/2)
                .userId(Long.valueOf(keyGenerator.generateKey().toString()))
                .build();

		orderProcessor.saveOrder();
		orderProcessor.saveOrderMerchantAsync();
        //orderService.save(orderPg);

        responseObserver.onNext(Order.newBuilder().setId(orderPg.getId()).build());
        responseObserver.onCompleted();
    }
    

	@Override
	public StreamObserver<Order> findByUserIdAsync(StreamObserver<Order> responseObserver) {
		return new StreamObserver<Order>() {
			
			List<Order> responseList = new ArrayList<Order>();
			
			@Override
			public void onNext(Order value) {
				
				com.paytm.pg.db.bean.Order orderBo =  orderService.findByUserId(value.getUserId());
				Order order = Order.newBuilder().setId(orderBo.getId()).setOrderId(orderBo.getOrderId())
						.setUserId(orderBo.getUserId()).build();

				responseList.add(order);
			}
			
			@Override
			public void onError(Throwable t) {
				responseObserver.onError(t);				
			}
			
			@Override
			public void onCompleted() {
				responseList.stream().forEach(responseObserver::onNext);
				responseObserver.onCompleted();
			}
		};
	}

	@Override
	public StreamObserver<Order> findByOrderIdAsync(StreamObserver<Order> responseObserver) {
		return new StreamObserver<Order>() {

			Order response = null;

			@Override
			public void onNext(Order value) {
				
				com.paytm.pg.db.bean.Order orderBo = orderService.findByOrderId(value.getOrderId());
				response = Order.newBuilder().setId(orderBo.getId()).setOrderId(orderBo.getOrderId())
						.setUserId(orderBo.getUserId()).build();
			}

			@Override
			public void onError(Throwable t) {
				responseObserver.onError(t);
			}

			@Override
			public void onCompleted() {
				responseObserver.onNext(response);
				responseObserver.onCompleted();
			}
		};
	}

}
