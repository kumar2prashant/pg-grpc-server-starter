package com.paytm.pg.db.controller;

import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paytm.pg.db.bean.Order;
import com.paytm.pg.db.service.OrderServiceDB;

@RestController
@RequestMapping("order")
public class OrderController {

    final SnowflakeShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();

    @Autowired
    private OrderServiceDB orderService;

    @GetMapping("save")
    public String save(Order order){
        Long id = Long.valueOf(keyGenerator.generateKey().toString());
        order.setId(id);
        order.setOrderId(id/2);
        order.setUserId(Long.valueOf(keyGenerator.generateKey().toString()));
        orderService.save(order);
        return "success";
    }

    @GetMapping("findById")
    public Order findById(Long id){
        return orderService.findById(618226345868001282L, 309112554276257792L);
    }


}
