package com.paytm.pg.db.controller;

import io.grpc.stub.StreamObserver;
import com.paytm.pg.TestingRequest;
import com.paytm.pg.TestingResponse;
import com.paytm.pg.TestingGrpc;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class TestGrpcController extends TestingGrpc.TestingImplBase {

    @Override
    public void testingRequestHandler(TestingRequest req, StreamObserver<TestingResponse> responseObserver) {
    	TestingResponse reply = TestingResponse.newBuilder().setResponse(req.getVarOne() + req.getVarTwo()).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
