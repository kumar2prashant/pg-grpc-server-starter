package com.paytm.pg.db.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.paytm.pg.db.bean.Order;

@Mapper
public interface OrderMapper {

    @Insert("insert into t_order values(#{id},#{orderId},#{userId},#{username})")
    public void insertOrder(Order order);

    @Select("select * from t_order where user_id=#{userId} and order_Id=#{orderId}")
    public Order findById(@Param("userId") long userId, @Param("orderId") long orderId);
    
    @Select("select * from t_order where user_id=#{userId}")
    public Order findByUserId(@Param("userId") long userId);
    
    @Select("select * from t_order where order_id=#{orderId}")
    public Order findByOrderId(@Param("orderId") long orderId);
}
