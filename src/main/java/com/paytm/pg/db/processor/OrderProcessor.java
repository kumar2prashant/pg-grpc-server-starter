package com.paytm.pg.db.processor;

import com.google.common.base.Stopwatch;
import com.paytm.pg.db.bean.Merchant;
import com.paytm.pg.db.service.MerchantService;
import com.paytm.pg.db.service.OrderServiceDB;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.core.strategy.keygen.SnowflakeShardingKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class OrderProcessor {

	@Autowired
    private OrderServiceDB orderService;
	
	@Autowired
    private MerchantService merchantService;
	
	final SnowflakeShardingKeyGenerator keyGenerator = new SnowflakeShardingKeyGenerator();
	
	public void saveMerchant(){
         merchantService.saveMerchant();
         log.info("merchant Saved");
    } 
	
	public void saveOrder(){
		Random rand = new Random();
		int data = rand.nextInt(14);
		int mid = 	data*2;
		int contractId = data*3;

		Stopwatch stopwatch = Stopwatch.createStarted();
		final Merchant merchant = merchantService.findByMerchantId(
				Merchant.builder()
						.mid((long)mid)
						.contractId((long)contractId)
						.build());
		
		log.info("Fetched merchant |" + merchant.toString());
		log.info("time1: " + stopwatch.stop());
		Long id = Long.valueOf(keyGenerator.generateKey().toString());
        com.paytm.pg.db.bean.Order orderPg = com.paytm.pg.db.bean.Order.builder()
                .id(id)
                .orderId(id/2)
                .userId(Long.valueOf(keyGenerator.generateKey().toString()))
                .build();
        
		orderService.save(orderPg);
        log.info("Order Saved");
   }
	
	@SneakyThrows
	public void saveOrderMerchantAsync(){
		Random rand = new Random();
		int data = rand.nextInt(14);
		int mid = 	data*2;
		int contractId = data*3;
		Stopwatch stopwatch = Stopwatch.createStarted();
		final Merchant merchant = merchantService.findByMerchantIdAsync(
				Merchant.builder()
						.mid((long)mid)
						.contractId((long)contractId)
						.build());
		log.info("saveOrderMerchantAsync" + merchant.toString());
		log.info("time Async: " + stopwatch.stop());
		
		Long id = Long.valueOf(keyGenerator.generateKey().toString());
        com.paytm.pg.db.bean.Order orderPg = com.paytm.pg.db.bean.Order.builder()
                .id(id)
                .orderId(id/2)
                .userId(Long.valueOf(keyGenerator.generateKey().toString()))
                .build();
        
		orderService.save(orderPg);
        log.info("merchant Saved");
   }
	
}
