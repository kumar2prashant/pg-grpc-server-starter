package com.paytm.pg.db.bean;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder
@ToString
@EqualsAndHashCode
public class Feature{
    public Point location;
    public String name;
    
    public io.grpc.examples.routeguide.Feature toProto() {
    	io.grpc.examples.routeguide.Feature feature = 
    			io.grpc.examples.routeguide.Feature.newBuilder()
    			.setName(this.getName())
    			.setLocation(this.getLocation().toProto()).build();
    	
    	return feature;
    }
}
