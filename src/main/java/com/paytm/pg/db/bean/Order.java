package com.paytm.pg.db.bean;

import lombok.Builder;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Builder
@Alias("t_order")
public class Order {
    private Long id;
    private Long orderId;
    private Long userId;
    private String username;
}