package com.paytm.pg.db.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Builder
public class MerchantGateways {

    private String gatewayName;

    private  String gatewayId;

    public com.paytm.pg.MerchantGateways toProto() {
        return com.paytm.pg.MerchantGateways.newBuilder()
                .setGatewayId(this.gatewayId)
                .setGatewayName(this.gatewayName)
                .build();
    }

    public static MerchantGateways fromProto(com.paytm.pg.MerchantGateways merchantGateways) {
        return MerchantGateways.builder()
                .gatewayName(merchantGateways.getGatewayName())
                .gatewayId(merchantGateways.getGatewayId())
                .build();
    }
}