package com.paytm.pg.db.bean;

import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Root{
    public List<Feature> feature;
}