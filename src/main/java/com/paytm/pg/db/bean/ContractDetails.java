package com.paytm.pg.db.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContractDetails {

    private String contractName;

    private  String productCode;

    public com.paytm.pg.ContractDetails toProto() {
        return com.paytm.pg.ContractDetails.newBuilder()
                .setContractName(this.contractName)
                .setProductCode(this.productCode)
                .build();
    }

    public static ContractDetails fromProto(com.paytm.pg.ContractDetails contractDetails) {
        return ContractDetails.builder()
                .contractName(contractDetails.getContractName())
                .contractName(contractDetails.getProductCode())
                .build();
    }
}