package com.paytm.pg.db.bean;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Point{
    public int latitude;
    public int longitude;
    
    public io.grpc.examples.routeguide.Point toProto() {
		return io.grpc.examples.routeguide.Point.newBuilder()
		.setLatitude(this.getLatitude())
		.setLongitude(this.getLongitude()).build();
    }
}