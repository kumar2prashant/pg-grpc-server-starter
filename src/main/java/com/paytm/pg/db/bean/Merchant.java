package com.paytm.pg.db.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Merchant{ // merchant -> proto Merchant

    private Long mid;

    private Long contractId;

    private ContractDetails contractDetails;

    private MerchantGateways merchantGateways;
    
    public com.paytm.pg.Merchant toProto() {
    	return com.paytm.pg.Merchant.newBuilder()
    	.setMid(this.mid)
    	.setContractId(this.getContractId())
    	.setContractDetails(this.contractDetails.toProto())
		.setMerchantGateways(this.merchantGateways.toProto())
				.build();

    }
    
    public static Merchant fromProto(com.paytm.pg.Merchant merchant) {
    	return Merchant.builder()
    	.mid(merchant.getMid())
    	.contractId(merchant.getContractId())
    	.contractDetails(ContractDetails.fromProto(merchant.getContractDetails()))
		.merchantGateways(MerchantGateways.fromProto(merchant.getMerchantGateways()))
    	.build();
    }
}