package com.paytm.pg.server.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class GracefulShutdown implements TomcatConnectorCustomizer, ApplicationListener<ContextClosedEvent> {
    @Value("${graceful-shutdown.tomcat.shutdown-grace-period-in-seconds}")
    private int shutdownGracePeriodInSeconds;

    @Value("${graceful-shutdown.tomcat.shutdown-now-grace-period-in-seconds}")
    private int shutdownNowGracePeriodInSeconds;

    @Value("${graceful-shutdown.tomcat.wait-before-close-in-seconds}")
    private int waitBeforeCloseInSeconds;

    private ApplicationContext context;

    private volatile Connector connector;

    private volatile Boolean isShuttingDown = false;

    public GracefulShutdown(ApplicationContext context) {
        this.context = context;
    }

    public Boolean isShuttingDown() {
        return isShuttingDown;
    }

    @Override
    public void customize(Connector connector) {
        this.connector = connector;
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        log.info("{}", event.getApplicationContext());

        isShuttingDown = true;

        if (event.getApplicationContext() == context)
        {
            log.info("Received ContextClosedEvent, start graceful shutdown");

            log.info("Wait {} seconds before stop accepting requests", waitBeforeCloseInSeconds);
            try {
                Thread.sleep(waitBeforeCloseInSeconds * 1000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt(); //restore the interruption status
                log.info("Wait before close gets interrupted: ", ex);
                log.info("We will continue the shutdown process,  some requests may be rejected");
            }
        } else{
            log.info("Received ContextClosedEvent from non-local context, skip it. {}", event.getApplicationContext());
            return;
        }
        this.connector.pause();
        Executor executor = this.connector.getProtocolHandler().getExecutor();
        if (executor instanceof ThreadPoolExecutor) {
            ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executor;
            log.info("ThreadPoolExecutor: {}", threadPoolExecutor.toString());
            try {
                // Disable new tasks from being submitted
                threadPoolExecutor.shutdown();
                log.info("ThreadPoolExecutor: shutdown");

                // Wait a while for existing tasks to terminate
                if (!threadPoolExecutor.awaitTermination(shutdownGracePeriodInSeconds, TimeUnit.SECONDS)) {
                    log.warn("ThreadPoolExecutor: Tomcat thread pool did not shut down gracefully within [{}] seconds. Proceeding with forceful shutdown", shutdownGracePeriodInSeconds);

                    // Cancel currently executing tasks
                    threadPoolExecutor.shutdownNow();
                    log.info("ThreadPoolExecutor: shutdownNow");

                    // Wait a while for tasks to respond to being cancelled
                    if (!threadPoolExecutor.awaitTermination(shutdownNowGracePeriodInSeconds, TimeUnit.SECONDS)) {
                        log.warn("DEBUG: ThreadPoolExecutor: Tomcat thread pool did not terminate within [{}] seconds.", shutdownNowGracePeriodInSeconds);
                    }
                }
            } catch (InterruptedException ex) {
                log.error("ThreadPoolExecutor: InterruptedException", ex);
                // (Re-)Cancel if current thread also interrupted
                threadPoolExecutor.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
        }
        log.info("Exit onApplicationEvent");
    }
}
