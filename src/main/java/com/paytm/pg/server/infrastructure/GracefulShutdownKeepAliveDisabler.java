package com.paytm.pg.server.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class GracefulShutdownKeepAliveDisabler extends OncePerRequestFilter {
    private static final String KEEP_ALIVE_HEADER = "Keep-Alive";

    private GracefulShutdown gracefulShutdown;

    @Value("${graceful-shutdown.tomcat.keep-alive-timeout-in-seconds}")
    private int keepAliveTimeoutInSeconds;

    public GracefulShutdownKeepAliveDisabler(GracefulShutdown gracefulShutdown) {
        this.gracefulShutdown = gracefulShutdown;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (gracefulShutdown.isShuttingDown()) {
            response.addHeader(HttpHeaders.CONNECTION, "close");
        } else {
            // the keep-alive header is not honored by all the http client
            // add 1s buffer because there can be a network lag
            response.addHeader(KEEP_ALIVE_HEADER, "timeout=" + (keepAliveTimeoutInSeconds > 1 ? keepAliveTimeoutInSeconds - 1 : 0));
        }
        filterChain.doFilter(request, response);
    }
}
