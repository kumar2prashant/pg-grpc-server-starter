package com.paytm.pg.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Getter
@Slf4j
public class CommonAppConfig {

    // in kafka property
    @Value(value = CommonConfigProperties.KF_BOOTSTRAP_ADD)
    private String kafkaBootstrapAdd;
    @Value(value = CommonConfigProperties.KAFKA_ENABLE_AUTO_COMMIT)
    private String kafkaEnableAutoCommit;
    @Value(value = CommonConfigProperties.KAFKA_MAX_POLL_RECORDS_FEED_DATA)
    private String kafkaMaxPollRecordsFeedOrders;
    @Value(value = CommonConfigProperties.KAFKA_CONCURRENCY_FEED_DATA)
    private int kafkaConcurrencyFeedOrders;
    @Value(value = CommonConfigProperties.KAFKA_ID_FEED_DATA)
    private String kafkaIdFeedData;
    @Value(CommonConfigProperties.KAFKA_POLL_INTERVAL_TIMEOUT_MS)
    private int kafkaPollIntervalTimeoutMs;
}
