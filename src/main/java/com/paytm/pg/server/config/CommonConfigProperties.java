package com.paytm.pg.server.config;

/**
 * @author akashbhardwaj
 */
public class CommonConfigProperties {

    public static final String KF_BOOTSTRAP_ADD                          = "${kafka.bootstrapAddress}";
    public static final String KAFKA_ENABLE_AUTO_COMMIT                  = "${kafka.enable.auto.commit}";
    public static final String KAFKA_MAX_POLL_RECORDS_FEED_DATA          = "${kafka.max.poll.records.feed.data}";

    public static final String KAFKA_POLL_INTERVAL_TIMEOUT_MS            = "${kafka.poll.interval.timeout.ms}";
    public static final String KAFKA_CONCURRENCY_FEED_DATA               = "${kafka.concurrency.feed.data}";

    // kafka Broker Id
    public static final String KAFKA_ID_FEED_DATA                        = "${kafka.id.feed.data}";

    // group
    public static final String KAFKA_TOPIC_GROUP_FEED_DATA               = "${kafka.topic.group.feed.data}";

}
