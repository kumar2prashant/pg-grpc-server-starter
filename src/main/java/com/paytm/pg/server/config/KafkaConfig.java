package com.paytm.pg.server.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;

@EnableKafka
@Configuration
public class KafkaConfig {

    @Autowired
    CommonAppConfig commonAppConfig;

    /**
     * Kafka listener container factory feed data concurrent kafka listener container factory.
     *
     * @return the concurrent kafka listener container factory
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaSingleListenerContainerFactory() {
        return getContainerFactory(commonAppConfig.getKafkaConcurrencyFeedOrders(),
            commonAppConfig.getKafkaMaxPollRecordsFeedOrders(),
            commonAppConfig.getKafkaPollIntervalTimeoutMs(), false);
    }

    /**
     * Kafka listener container factory feed data concurrent kafka listener container factory.
     *
     * @return the concurrent kafka listener container factory
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactoryFeedData() {
        return getContainerFactory(commonAppConfig.getKafkaConcurrencyFeedOrders(),
            commonAppConfig.getKafkaMaxPollRecordsFeedOrders(),
            commonAppConfig.getKafkaPollIntervalTimeoutMs(), true);
    }

    /**
     * Consumer factory consumer factory.
     *
     * @param maxPollRecords the max poll records
     * @return the consumer factory
     */
    @Bean
    @Scope("prototype")
    public ConsumerFactory<String, String> consumerFactory(String maxPollRecords,
                                                                int maxIntervalPollRecord) {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, commonAppConfig.getKafkaBootstrapAdd());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,
            commonAppConfig.getKafkaEnableAutoCommit());
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, maxIntervalPollRecord);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);

        return new DefaultKafkaConsumerFactory<>(props);
    }

    private ConcurrentKafkaListenerContainerFactory<String, String> getContainerFactory(int concurrency,
                                                                                             String maxPollRecords,
                                                                                             int maxInterval,
                                                                                             boolean batchListener) {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory(maxPollRecords, maxInterval));
        factory.setConcurrency(concurrency);
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL);
        factory.setBatchListener(batchListener);
        return factory;
    }

}
