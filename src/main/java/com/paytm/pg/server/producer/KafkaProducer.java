package com.paytm.pg.server.producer;

import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import lombok.extern.slf4j.Slf4j;

@Component
@EnableKafka
@Slf4j
public class KafkaProducer {

    @Autowired
    @Qualifier("avroKafkaTemplate")
    private KafkaTemplate<String, String> avroKafkaTemplate;

    /**
     *
     * @param topic       the topic
     * @param key         the key
     * @param unifiedData the unified data
     */
    public void publishMessage(String topic, String key, String unifiedData) {
        try {
                avroKafkaTemplate
                        .send(topic, key, unifiedData).get(10L, TimeUnit.SECONDS);
            log.info("SendingMessage" + " [" + topic + "][" + key + "][" + unifiedData + "]");

    } catch (Exception exc) {
    		log.error("Error while publishing", exc);
        }
    }

    /***
     * message publisher
     * @param record
     */
    public void publishMessage(ProducerRecord record)  {
        try {
            avroKafkaTemplate.send(record)
                    .get(10L, TimeUnit.SECONDS);
            log.info("SendingMessage" + " [" + record + "]");
        } catch (Exception exc) {
        	log.error("Error while publishing", exc);
        }
    }
    
    public void sendMessage(String topic, String key, String unifiedData) {
        ListenableFuture<SendResult<String, String>> future = avroKafkaTemplate.send(topic, unifiedData);
        
    future.addCallback(
            new ListenableFutureCallback<SendResult<String, String>>() {
              
              @Override
              public void onSuccess(SendResult<String, String> result) {
                log.info(
                    "Sent message=[{}] with offset=[{}]", unifiedData, result.getRecordMetadata().offset());
              }

              @Override
              public void onFailure(Throwable ex) {
                log.info("Unable to send message=[{}] due to : {}", unifiedData, ex.getMessage());
              }
              
            });
    }
       
}
