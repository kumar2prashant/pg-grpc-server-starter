package com.paytm.pg.server.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.paytm.pgplus.common.avro.message.AvroMessage;

public class JacksonUtility {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static <T> T readValue(String content, Class<T> valueType) throws JsonProcessingException {
        return mapper.readValue(content, valueType);
    }

//    public static <T> T readValueFromAvroMessage(AvroMessage message, Class<T> valueType) throws JsonProcessingException {
//        return mapper.readValue(message.getValue().toString(), valueType);
//    }

    /**
     * To json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String toJsonString(Object obj) throws JsonProcessingException {
            return mapper.writeValueAsString(obj);
    }

    /**
     * Gets from json string.
     *
     * @param <T>       the type parameter
     * @param jsonStr   the json str
     * @param valueType the value type
     * @param key       the key
     * @return the from json string
     */
    public static <T> T getFromJsonString(String jsonStr, Class<T> valueType, String key) throws JsonProcessingException {
        if (jsonStr == null || jsonStr.trim().isEmpty()) {
            return null;
        }
            return mapper.readValue(jsonStr, valueType);
    }

}
