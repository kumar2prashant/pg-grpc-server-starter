package com.paytm.pg;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * The Employee Service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: Order.proto")
public final class OrderServiceGrpc {

  private OrderServiceGrpc() {}

  public static final String SERVICE_NAME = "com.paytm.pg.OrderService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getSaveOrderMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "saveOrder",
      requestType = com.paytm.pg.Order.class,
      responseType = com.paytm.pg.Order.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getSaveOrderMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Order, com.paytm.pg.Order> getSaveOrderMethod;
    if ((getSaveOrderMethod = OrderServiceGrpc.getSaveOrderMethod) == null) {
      synchronized (OrderServiceGrpc.class) {
        if ((getSaveOrderMethod = OrderServiceGrpc.getSaveOrderMethod) == null) {
          OrderServiceGrpc.getSaveOrderMethod = getSaveOrderMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Order, com.paytm.pg.Order>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "saveOrder"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setSchemaDescriptor(new OrderServiceMethodDescriptorSupplier("saveOrder"))
              .build();
        }
      }
    }
    return getSaveOrderMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getSaveOrderV1Method;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "saveOrderV1",
      requestType = com.paytm.pg.Order.class,
      responseType = com.paytm.pg.Order.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getSaveOrderV1Method() {
    io.grpc.MethodDescriptor<com.paytm.pg.Order, com.paytm.pg.Order> getSaveOrderV1Method;
    if ((getSaveOrderV1Method = OrderServiceGrpc.getSaveOrderV1Method) == null) {
      synchronized (OrderServiceGrpc.class) {
        if ((getSaveOrderV1Method = OrderServiceGrpc.getSaveOrderV1Method) == null) {
          OrderServiceGrpc.getSaveOrderV1Method = getSaveOrderV1Method =
              io.grpc.MethodDescriptor.<com.paytm.pg.Order, com.paytm.pg.Order>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "saveOrderV1"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setSchemaDescriptor(new OrderServiceMethodDescriptorSupplier("saveOrderV1"))
              .build();
        }
      }
    }
    return getSaveOrderV1Method;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByUserIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByUserId",
      requestType = com.paytm.pg.Order.class,
      responseType = com.paytm.pg.Order.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByUserIdMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Order, com.paytm.pg.Order> getFindByUserIdMethod;
    if ((getFindByUserIdMethod = OrderServiceGrpc.getFindByUserIdMethod) == null) {
      synchronized (OrderServiceGrpc.class) {
        if ((getFindByUserIdMethod = OrderServiceGrpc.getFindByUserIdMethod) == null) {
          OrderServiceGrpc.getFindByUserIdMethod = getFindByUserIdMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Order, com.paytm.pg.Order>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByUserId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setSchemaDescriptor(new OrderServiceMethodDescriptorSupplier("findByUserId"))
              .build();
        }
      }
    }
    return getFindByUserIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByOrderIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByOrderId",
      requestType = com.paytm.pg.Order.class,
      responseType = com.paytm.pg.Order.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByOrderIdMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Order, com.paytm.pg.Order> getFindByOrderIdMethod;
    if ((getFindByOrderIdMethod = OrderServiceGrpc.getFindByOrderIdMethod) == null) {
      synchronized (OrderServiceGrpc.class) {
        if ((getFindByOrderIdMethod = OrderServiceGrpc.getFindByOrderIdMethod) == null) {
          OrderServiceGrpc.getFindByOrderIdMethod = getFindByOrderIdMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Order, com.paytm.pg.Order>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByOrderId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setSchemaDescriptor(new OrderServiceMethodDescriptorSupplier("findByOrderId"))
              .build();
        }
      }
    }
    return getFindByOrderIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByUserIdAsyncMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByUserIdAsync",
      requestType = com.paytm.pg.Order.class,
      responseType = com.paytm.pg.Order.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByUserIdAsyncMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Order, com.paytm.pg.Order> getFindByUserIdAsyncMethod;
    if ((getFindByUserIdAsyncMethod = OrderServiceGrpc.getFindByUserIdAsyncMethod) == null) {
      synchronized (OrderServiceGrpc.class) {
        if ((getFindByUserIdAsyncMethod = OrderServiceGrpc.getFindByUserIdAsyncMethod) == null) {
          OrderServiceGrpc.getFindByUserIdAsyncMethod = getFindByUserIdAsyncMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Order, com.paytm.pg.Order>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByUserIdAsync"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setSchemaDescriptor(new OrderServiceMethodDescriptorSupplier("findByUserIdAsync"))
              .build();
        }
      }
    }
    return getFindByUserIdAsyncMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByOrderIdAsyncMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByOrderIdAsync",
      requestType = com.paytm.pg.Order.class,
      responseType = com.paytm.pg.Order.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Order,
      com.paytm.pg.Order> getFindByOrderIdAsyncMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Order, com.paytm.pg.Order> getFindByOrderIdAsyncMethod;
    if ((getFindByOrderIdAsyncMethod = OrderServiceGrpc.getFindByOrderIdAsyncMethod) == null) {
      synchronized (OrderServiceGrpc.class) {
        if ((getFindByOrderIdAsyncMethod = OrderServiceGrpc.getFindByOrderIdAsyncMethod) == null) {
          OrderServiceGrpc.getFindByOrderIdAsyncMethod = getFindByOrderIdAsyncMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Order, com.paytm.pg.Order>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByOrderIdAsync"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Order.getDefaultInstance()))
              .setSchemaDescriptor(new OrderServiceMethodDescriptorSupplier("findByOrderIdAsync"))
              .build();
        }
      }
    }
    return getFindByOrderIdAsyncMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static OrderServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<OrderServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<OrderServiceStub>() {
        @java.lang.Override
        public OrderServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new OrderServiceStub(channel, callOptions);
        }
      };
    return OrderServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static OrderServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<OrderServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<OrderServiceBlockingStub>() {
        @java.lang.Override
        public OrderServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new OrderServiceBlockingStub(channel, callOptions);
        }
      };
    return OrderServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static OrderServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<OrderServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<OrderServiceFutureStub>() {
        @java.lang.Override
        public OrderServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new OrderServiceFutureStub(channel, callOptions);
        }
      };
    return OrderServiceFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static abstract class OrderServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void saveOrder(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSaveOrderMethod(), responseObserver);
    }

    /**
     */
    public void saveOrderV1(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSaveOrderV1Method(), responseObserver);
    }

    /**
     */
    public void findByUserId(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindByUserIdMethod(), responseObserver);
    }

    /**
     */
    public void findByOrderId(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindByOrderIdMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Order> findByUserIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getFindByUserIdAsyncMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Order> findByOrderIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getFindByOrderIdAsyncMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSaveOrderMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Order,
                com.paytm.pg.Order>(
                  this, METHODID_SAVE_ORDER)))
          .addMethod(
            getSaveOrderV1Method(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Order,
                com.paytm.pg.Order>(
                  this, METHODID_SAVE_ORDER_V1)))
          .addMethod(
            getFindByUserIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Order,
                com.paytm.pg.Order>(
                  this, METHODID_FIND_BY_USER_ID)))
          .addMethod(
            getFindByOrderIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Order,
                com.paytm.pg.Order>(
                  this, METHODID_FIND_BY_ORDER_ID)))
          .addMethod(
            getFindByUserIdAsyncMethod(),
            io.grpc.stub.ServerCalls.asyncBidiStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Order,
                com.paytm.pg.Order>(
                  this, METHODID_FIND_BY_USER_ID_ASYNC)))
          .addMethod(
            getFindByOrderIdAsyncMethod(),
            io.grpc.stub.ServerCalls.asyncClientStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Order,
                com.paytm.pg.Order>(
                  this, METHODID_FIND_BY_ORDER_ID_ASYNC)))
          .build();
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class OrderServiceStub extends io.grpc.stub.AbstractAsyncStub<OrderServiceStub> {
    private OrderServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrderServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new OrderServiceStub(channel, callOptions);
    }

    /**
     */
    public void saveOrder(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSaveOrderMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void saveOrderV1(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSaveOrderV1Method(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findByUserId(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindByUserIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findByOrderId(com.paytm.pg.Order request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindByOrderIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Order> findByUserIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncBidiStreamingCall(
          getChannel().newCall(getFindByUserIdAsyncMethod(), getCallOptions()), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Order> findByOrderIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Order> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncClientStreamingCall(
          getChannel().newCall(getFindByOrderIdAsyncMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class OrderServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<OrderServiceBlockingStub> {
    private OrderServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrderServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new OrderServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.paytm.pg.Order saveOrder(com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSaveOrderMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.paytm.pg.Order saveOrderV1(com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSaveOrderV1Method(), getCallOptions(), request);
    }

    /**
     */
    public com.paytm.pg.Order findByUserId(com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindByUserIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.paytm.pg.Order findByOrderId(com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindByOrderIdMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class OrderServiceFutureStub extends io.grpc.stub.AbstractFutureStub<OrderServiceFutureStub> {
    private OrderServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrderServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new OrderServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Order> saveOrder(
        com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSaveOrderMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Order> saveOrderV1(
        com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSaveOrderV1Method(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Order> findByUserId(
        com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindByUserIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Order> findByOrderId(
        com.paytm.pg.Order request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindByOrderIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SAVE_ORDER = 0;
  private static final int METHODID_SAVE_ORDER_V1 = 1;
  private static final int METHODID_FIND_BY_USER_ID = 2;
  private static final int METHODID_FIND_BY_ORDER_ID = 3;
  private static final int METHODID_FIND_BY_USER_ID_ASYNC = 4;
  private static final int METHODID_FIND_BY_ORDER_ID_ASYNC = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final OrderServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(OrderServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SAVE_ORDER:
          serviceImpl.saveOrder((com.paytm.pg.Order) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Order>) responseObserver);
          break;
        case METHODID_SAVE_ORDER_V1:
          serviceImpl.saveOrderV1((com.paytm.pg.Order) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Order>) responseObserver);
          break;
        case METHODID_FIND_BY_USER_ID:
          serviceImpl.findByUserId((com.paytm.pg.Order) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Order>) responseObserver);
          break;
        case METHODID_FIND_BY_ORDER_ID:
          serviceImpl.findByOrderId((com.paytm.pg.Order) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Order>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FIND_BY_USER_ID_ASYNC:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.findByUserIdAsync(
              (io.grpc.stub.StreamObserver<com.paytm.pg.Order>) responseObserver);
        case METHODID_FIND_BY_ORDER_ID_ASYNC:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.findByOrderIdAsync(
              (io.grpc.stub.StreamObserver<com.paytm.pg.Order>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class OrderServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    OrderServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.paytm.pg.OrderOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("OrderService");
    }
  }

  private static final class OrderServiceFileDescriptorSupplier
      extends OrderServiceBaseDescriptorSupplier {
    OrderServiceFileDescriptorSupplier() {}
  }

  private static final class OrderServiceMethodDescriptorSupplier
      extends OrderServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    OrderServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (OrderServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new OrderServiceFileDescriptorSupplier())
              .addMethod(getSaveOrderMethod())
              .addMethod(getSaveOrderV1Method())
              .addMethod(getFindByUserIdMethod())
              .addMethod(getFindByOrderIdMethod())
              .addMethod(getFindByUserIdAsyncMethod())
              .addMethod(getFindByOrderIdAsyncMethod())
              .build();
        }
      }
    }
    return result;
  }
}
