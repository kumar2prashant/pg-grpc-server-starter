package com.paytm.pg;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * The Employee Service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: Employee.proto")
public final class EmployeeServiceGrpc {

  private EmployeeServiceGrpc() {}

  public static final String SERVICE_NAME = "com.paytm.pg.EmployeeService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Employee,
      com.paytm.pg.Employee> getGetEmployeeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getEmployee",
      requestType = com.paytm.pg.Employee.class,
      responseType = com.paytm.pg.Employee.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Employee,
      com.paytm.pg.Employee> getGetEmployeeMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Employee, com.paytm.pg.Employee> getGetEmployeeMethod;
    if ((getGetEmployeeMethod = EmployeeServiceGrpc.getGetEmployeeMethod) == null) {
      synchronized (EmployeeServiceGrpc.class) {
        if ((getGetEmployeeMethod = EmployeeServiceGrpc.getGetEmployeeMethod) == null) {
          EmployeeServiceGrpc.getGetEmployeeMethod = getGetEmployeeMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Employee, com.paytm.pg.Employee>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getEmployee"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Employee.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Employee.getDefaultInstance()))
              .setSchemaDescriptor(new EmployeeServiceMethodDescriptorSupplier("getEmployee"))
              .build();
        }
      }
    }
    return getGetEmployeeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Employee,
      com.paytm.pg.Employee> getGetAllEmployeesByIDListMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllEmployeesByIDList",
      requestType = com.paytm.pg.Employee.class,
      responseType = com.paytm.pg.Employee.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Employee,
      com.paytm.pg.Employee> getGetAllEmployeesByIDListMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Employee, com.paytm.pg.Employee> getGetAllEmployeesByIDListMethod;
    if ((getGetAllEmployeesByIDListMethod = EmployeeServiceGrpc.getGetAllEmployeesByIDListMethod) == null) {
      synchronized (EmployeeServiceGrpc.class) {
        if ((getGetAllEmployeesByIDListMethod = EmployeeServiceGrpc.getGetAllEmployeesByIDListMethod) == null) {
          EmployeeServiceGrpc.getGetAllEmployeesByIDListMethod = getGetAllEmployeesByIDListMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Employee, com.paytm.pg.Employee>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getAllEmployeesByIDList"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Employee.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Employee.getDefaultInstance()))
              .setSchemaDescriptor(new EmployeeServiceMethodDescriptorSupplier("getAllEmployeesByIDList"))
              .build();
        }
      }
    }
    return getGetAllEmployeesByIDListMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Employee,
      com.paytm.pg.Employee> getGetMostExperiencedEmployeeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getMostExperiencedEmployee",
      requestType = com.paytm.pg.Employee.class,
      responseType = com.paytm.pg.Employee.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Employee,
      com.paytm.pg.Employee> getGetMostExperiencedEmployeeMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Employee, com.paytm.pg.Employee> getGetMostExperiencedEmployeeMethod;
    if ((getGetMostExperiencedEmployeeMethod = EmployeeServiceGrpc.getGetMostExperiencedEmployeeMethod) == null) {
      synchronized (EmployeeServiceGrpc.class) {
        if ((getGetMostExperiencedEmployeeMethod = EmployeeServiceGrpc.getGetMostExperiencedEmployeeMethod) == null) {
          EmployeeServiceGrpc.getGetMostExperiencedEmployeeMethod = getGetMostExperiencedEmployeeMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Employee, com.paytm.pg.Employee>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getMostExperiencedEmployee"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Employee.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Employee.getDefaultInstance()))
              .setSchemaDescriptor(new EmployeeServiceMethodDescriptorSupplier("getMostExperiencedEmployee"))
              .build();
        }
      }
    }
    return getGetMostExperiencedEmployeeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static EmployeeServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<EmployeeServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<EmployeeServiceStub>() {
        @java.lang.Override
        public EmployeeServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new EmployeeServiceStub(channel, callOptions);
        }
      };
    return EmployeeServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static EmployeeServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<EmployeeServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<EmployeeServiceBlockingStub>() {
        @java.lang.Override
        public EmployeeServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new EmployeeServiceBlockingStub(channel, callOptions);
        }
      };
    return EmployeeServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static EmployeeServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<EmployeeServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<EmployeeServiceFutureStub>() {
        @java.lang.Override
        public EmployeeServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new EmployeeServiceFutureStub(channel, callOptions);
        }
      };
    return EmployeeServiceFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static abstract class EmployeeServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Get employee By Employee ID
     * IDL of unary RPCs where the client sends a single request to the server and gets a single response back, just like a normal function call.
     * </pre>
     */
    public void getEmployee(com.paytm.pg.Employee request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Employee> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetEmployeeMethod(), responseObserver);
    }

    /**
     * <pre>
     * Get all employees by any given name
     * IDL of bidirectional streaming RPCs where both sides send a sequence of messages using a read-write stream. The two streams operate independently, so clients and servers can read and write in whatever order they like: for example, the server could wait to receive all the client messages before writing its responses, or it could alternately read a message then write a message, or some other combination of reads and writes. The order of messages in each stream is preserved.
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Employee> getAllEmployeesByIDList(
        io.grpc.stub.StreamObserver<com.paytm.pg.Employee> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getGetAllEmployeesByIDListMethod(), responseObserver);
    }

    /**
     * <pre>
     * Get employee By Employee ID
     * IDL of client streaming RPCs where the client writes a sequence of messages and sends them to the server, again using a provided stream. Once the client has finished writing the messages, it waits for the server to read them and return its response. Again gRPC guarantees message ordering within an individual RPC call.
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Employee> getMostExperiencedEmployee(
        io.grpc.stub.StreamObserver<com.paytm.pg.Employee> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getGetMostExperiencedEmployeeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetEmployeeMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Employee,
                com.paytm.pg.Employee>(
                  this, METHODID_GET_EMPLOYEE)))
          .addMethod(
            getGetAllEmployeesByIDListMethod(),
            io.grpc.stub.ServerCalls.asyncBidiStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Employee,
                com.paytm.pg.Employee>(
                  this, METHODID_GET_ALL_EMPLOYEES_BY_IDLIST)))
          .addMethod(
            getGetMostExperiencedEmployeeMethod(),
            io.grpc.stub.ServerCalls.asyncClientStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Employee,
                com.paytm.pg.Employee>(
                  this, METHODID_GET_MOST_EXPERIENCED_EMPLOYEE)))
          .build();
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class EmployeeServiceStub extends io.grpc.stub.AbstractAsyncStub<EmployeeServiceStub> {
    private EmployeeServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EmployeeServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new EmployeeServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * Get employee By Employee ID
     * IDL of unary RPCs where the client sends a single request to the server and gets a single response back, just like a normal function call.
     * </pre>
     */
    public void getEmployee(com.paytm.pg.Employee request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Employee> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetEmployeeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Get all employees by any given name
     * IDL of bidirectional streaming RPCs where both sides send a sequence of messages using a read-write stream. The two streams operate independently, so clients and servers can read and write in whatever order they like: for example, the server could wait to receive all the client messages before writing its responses, or it could alternately read a message then write a message, or some other combination of reads and writes. The order of messages in each stream is preserved.
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Employee> getAllEmployeesByIDList(
        io.grpc.stub.StreamObserver<com.paytm.pg.Employee> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncBidiStreamingCall(
          getChannel().newCall(getGetAllEmployeesByIDListMethod(), getCallOptions()), responseObserver);
    }

    /**
     * <pre>
     * Get employee By Employee ID
     * IDL of client streaming RPCs where the client writes a sequence of messages and sends them to the server, again using a provided stream. Once the client has finished writing the messages, it waits for the server to read them and return its response. Again gRPC guarantees message ordering within an individual RPC call.
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Employee> getMostExperiencedEmployee(
        io.grpc.stub.StreamObserver<com.paytm.pg.Employee> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncClientStreamingCall(
          getChannel().newCall(getGetMostExperiencedEmployeeMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class EmployeeServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<EmployeeServiceBlockingStub> {
    private EmployeeServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EmployeeServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new EmployeeServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Get employee By Employee ID
     * IDL of unary RPCs where the client sends a single request to the server and gets a single response back, just like a normal function call.
     * </pre>
     */
    public com.paytm.pg.Employee getEmployee(com.paytm.pg.Employee request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetEmployeeMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class EmployeeServiceFutureStub extends io.grpc.stub.AbstractFutureStub<EmployeeServiceFutureStub> {
    private EmployeeServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected EmployeeServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new EmployeeServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Get employee By Employee ID
     * IDL of unary RPCs where the client sends a single request to the server and gets a single response back, just like a normal function call.
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Employee> getEmployee(
        com.paytm.pg.Employee request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetEmployeeMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_EMPLOYEE = 0;
  private static final int METHODID_GET_ALL_EMPLOYEES_BY_IDLIST = 1;
  private static final int METHODID_GET_MOST_EXPERIENCED_EMPLOYEE = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final EmployeeServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(EmployeeServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_EMPLOYEE:
          serviceImpl.getEmployee((com.paytm.pg.Employee) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Employee>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALL_EMPLOYEES_BY_IDLIST:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.getAllEmployeesByIDList(
              (io.grpc.stub.StreamObserver<com.paytm.pg.Employee>) responseObserver);
        case METHODID_GET_MOST_EXPERIENCED_EMPLOYEE:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.getMostExperiencedEmployee(
              (io.grpc.stub.StreamObserver<com.paytm.pg.Employee>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class EmployeeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    EmployeeServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.paytm.pg.EmployeeOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("EmployeeService");
    }
  }

  private static final class EmployeeServiceFileDescriptorSupplier
      extends EmployeeServiceBaseDescriptorSupplier {
    EmployeeServiceFileDescriptorSupplier() {}
  }

  private static final class EmployeeServiceMethodDescriptorSupplier
      extends EmployeeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    EmployeeServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (EmployeeServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new EmployeeServiceFileDescriptorSupplier())
              .addMethod(getGetEmployeeMethod())
              .addMethod(getGetAllEmployeesByIDListMethod())
              .addMethod(getGetMostExperiencedEmployeeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
