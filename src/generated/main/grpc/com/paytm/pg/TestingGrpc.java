package com.paytm.pg;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: TestingService.proto")
public final class TestingGrpc {

  private TestingGrpc() {}

  public static final String SERVICE_NAME = "test.Testing";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.TestingRequest,
      com.paytm.pg.TestingResponse> getTestingRequestHandlerMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "TestingRequestHandler",
      requestType = com.paytm.pg.TestingRequest.class,
      responseType = com.paytm.pg.TestingResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.TestingRequest,
      com.paytm.pg.TestingResponse> getTestingRequestHandlerMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.TestingRequest, com.paytm.pg.TestingResponse> getTestingRequestHandlerMethod;
    if ((getTestingRequestHandlerMethod = TestingGrpc.getTestingRequestHandlerMethod) == null) {
      synchronized (TestingGrpc.class) {
        if ((getTestingRequestHandlerMethod = TestingGrpc.getTestingRequestHandlerMethod) == null) {
          TestingGrpc.getTestingRequestHandlerMethod = getTestingRequestHandlerMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.TestingRequest, com.paytm.pg.TestingResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "TestingRequestHandler"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.TestingRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.TestingResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TestingMethodDescriptorSupplier("TestingRequestHandler"))
              .build();
        }
      }
    }
    return getTestingRequestHandlerMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static TestingStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TestingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TestingStub>() {
        @java.lang.Override
        public TestingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TestingStub(channel, callOptions);
        }
      };
    return TestingStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static TestingBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TestingBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TestingBlockingStub>() {
        @java.lang.Override
        public TestingBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TestingBlockingStub(channel, callOptions);
        }
      };
    return TestingBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static TestingFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TestingFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TestingFutureStub>() {
        @java.lang.Override
        public TestingFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TestingFutureStub(channel, callOptions);
        }
      };
    return TestingFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class TestingImplBase implements io.grpc.BindableService {

    /**
     */
    public void testingRequestHandler(com.paytm.pg.TestingRequest request,
        io.grpc.stub.StreamObserver<com.paytm.pg.TestingResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getTestingRequestHandlerMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getTestingRequestHandlerMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.TestingRequest,
                com.paytm.pg.TestingResponse>(
                  this, METHODID_TESTING_REQUEST_HANDLER)))
          .build();
    }
  }

  /**
   */
  public static final class TestingStub extends io.grpc.stub.AbstractAsyncStub<TestingStub> {
    private TestingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TestingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TestingStub(channel, callOptions);
    }

    /**
     */
    public void testingRequestHandler(com.paytm.pg.TestingRequest request,
        io.grpc.stub.StreamObserver<com.paytm.pg.TestingResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getTestingRequestHandlerMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class TestingBlockingStub extends io.grpc.stub.AbstractBlockingStub<TestingBlockingStub> {
    private TestingBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TestingBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TestingBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.paytm.pg.TestingResponse testingRequestHandler(com.paytm.pg.TestingRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getTestingRequestHandlerMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class TestingFutureStub extends io.grpc.stub.AbstractFutureStub<TestingFutureStub> {
    private TestingFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TestingFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TestingFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.TestingResponse> testingRequestHandler(
        com.paytm.pg.TestingRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getTestingRequestHandlerMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_TESTING_REQUEST_HANDLER = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final TestingImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(TestingImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_TESTING_REQUEST_HANDLER:
          serviceImpl.testingRequestHandler((com.paytm.pg.TestingRequest) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.TestingResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class TestingBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    TestingBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.paytm.pg.TestingServiceProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Testing");
    }
  }

  private static final class TestingFileDescriptorSupplier
      extends TestingBaseDescriptorSupplier {
    TestingFileDescriptorSupplier() {}
  }

  private static final class TestingMethodDescriptorSupplier
      extends TestingBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    TestingMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (TestingGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new TestingFileDescriptorSupplier())
              .addMethod(getTestingRequestHandlerMethod())
              .build();
        }
      }
    }
    return result;
  }
}
