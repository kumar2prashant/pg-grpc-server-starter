package com.paytm.pg;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * The Employee Service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: Merchant.proto")
public final class MerchantServiceGrpc {

  private MerchantServiceGrpc() {}

  public static final String SERVICE_NAME = "com.paytm.pg.MerchantService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getSaveMerchantMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "saveMerchant",
      requestType = com.paytm.pg.Merchant.class,
      responseType = com.paytm.pg.Merchant.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getSaveMerchantMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Merchant, com.paytm.pg.Merchant> getSaveMerchantMethod;
    if ((getSaveMerchantMethod = MerchantServiceGrpc.getSaveMerchantMethod) == null) {
      synchronized (MerchantServiceGrpc.class) {
        if ((getSaveMerchantMethod = MerchantServiceGrpc.getSaveMerchantMethod) == null) {
          MerchantServiceGrpc.getSaveMerchantMethod = getSaveMerchantMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Merchant, com.paytm.pg.Merchant>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "saveMerchant"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setSchemaDescriptor(new MerchantServiceMethodDescriptorSupplier("saveMerchant"))
              .build();
        }
      }
    }
    return getSaveMerchantMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByMerchantIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByMerchantId",
      requestType = com.paytm.pg.Merchant.class,
      responseType = com.paytm.pg.Merchant.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByMerchantIdMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Merchant, com.paytm.pg.Merchant> getFindByMerchantIdMethod;
    if ((getFindByMerchantIdMethod = MerchantServiceGrpc.getFindByMerchantIdMethod) == null) {
      synchronized (MerchantServiceGrpc.class) {
        if ((getFindByMerchantIdMethod = MerchantServiceGrpc.getFindByMerchantIdMethod) == null) {
          MerchantServiceGrpc.getFindByMerchantIdMethod = getFindByMerchantIdMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Merchant, com.paytm.pg.Merchant>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByMerchantId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setSchemaDescriptor(new MerchantServiceMethodDescriptorSupplier("findByMerchantId"))
              .build();
        }
      }
    }
    return getFindByMerchantIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByContractIdMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByContractId",
      requestType = com.paytm.pg.Merchant.class,
      responseType = com.paytm.pg.Merchant.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByContractIdMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Merchant, com.paytm.pg.Merchant> getFindByContractIdMethod;
    if ((getFindByContractIdMethod = MerchantServiceGrpc.getFindByContractIdMethod) == null) {
      synchronized (MerchantServiceGrpc.class) {
        if ((getFindByContractIdMethod = MerchantServiceGrpc.getFindByContractIdMethod) == null) {
          MerchantServiceGrpc.getFindByContractIdMethod = getFindByContractIdMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Merchant, com.paytm.pg.Merchant>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByContractId"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setSchemaDescriptor(new MerchantServiceMethodDescriptorSupplier("findByContractId"))
              .build();
        }
      }
    }
    return getFindByContractIdMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByMerchantIdAsyncMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByMerchantIdAsync",
      requestType = com.paytm.pg.Merchant.class,
      responseType = com.paytm.pg.Merchant.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByMerchantIdAsyncMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Merchant, com.paytm.pg.Merchant> getFindByMerchantIdAsyncMethod;
    if ((getFindByMerchantIdAsyncMethod = MerchantServiceGrpc.getFindByMerchantIdAsyncMethod) == null) {
      synchronized (MerchantServiceGrpc.class) {
        if ((getFindByMerchantIdAsyncMethod = MerchantServiceGrpc.getFindByMerchantIdAsyncMethod) == null) {
          MerchantServiceGrpc.getFindByMerchantIdAsyncMethod = getFindByMerchantIdAsyncMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Merchant, com.paytm.pg.Merchant>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByMerchantIdAsync"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setSchemaDescriptor(new MerchantServiceMethodDescriptorSupplier("findByMerchantIdAsync"))
              .build();
        }
      }
    }
    return getFindByMerchantIdAsyncMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByContractIdAsyncMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "findByContractIdAsync",
      requestType = com.paytm.pg.Merchant.class,
      responseType = com.paytm.pg.Merchant.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Merchant,
      com.paytm.pg.Merchant> getFindByContractIdAsyncMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Merchant, com.paytm.pg.Merchant> getFindByContractIdAsyncMethod;
    if ((getFindByContractIdAsyncMethod = MerchantServiceGrpc.getFindByContractIdAsyncMethod) == null) {
      synchronized (MerchantServiceGrpc.class) {
        if ((getFindByContractIdAsyncMethod = MerchantServiceGrpc.getFindByContractIdAsyncMethod) == null) {
          MerchantServiceGrpc.getFindByContractIdAsyncMethod = getFindByContractIdAsyncMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Merchant, com.paytm.pg.Merchant>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "findByContractIdAsync"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Merchant.getDefaultInstance()))
              .setSchemaDescriptor(new MerchantServiceMethodDescriptorSupplier("findByContractIdAsync"))
              .build();
        }
      }
    }
    return getFindByContractIdAsyncMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MerchantServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MerchantServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MerchantServiceStub>() {
        @java.lang.Override
        public MerchantServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MerchantServiceStub(channel, callOptions);
        }
      };
    return MerchantServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MerchantServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MerchantServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MerchantServiceBlockingStub>() {
        @java.lang.Override
        public MerchantServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MerchantServiceBlockingStub(channel, callOptions);
        }
      };
    return MerchantServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MerchantServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MerchantServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MerchantServiceFutureStub>() {
        @java.lang.Override
        public MerchantServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MerchantServiceFutureStub(channel, callOptions);
        }
      };
    return MerchantServiceFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static abstract class MerchantServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void saveMerchant(com.paytm.pg.Merchant request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSaveMerchantMethod(), responseObserver);
    }

    /**
     */
    public void findByMerchantId(com.paytm.pg.Merchant request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindByMerchantIdMethod(), responseObserver);
    }

    /**
     */
    public void findByContractId(com.paytm.pg.Merchant request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getFindByContractIdMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> findByMerchantIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getFindByMerchantIdAsyncMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> findByContractIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getFindByContractIdAsyncMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSaveMerchantMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Merchant,
                com.paytm.pg.Merchant>(
                  this, METHODID_SAVE_MERCHANT)))
          .addMethod(
            getFindByMerchantIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Merchant,
                com.paytm.pg.Merchant>(
                  this, METHODID_FIND_BY_MERCHANT_ID)))
          .addMethod(
            getFindByContractIdMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.paytm.pg.Merchant,
                com.paytm.pg.Merchant>(
                  this, METHODID_FIND_BY_CONTRACT_ID)))
          .addMethod(
            getFindByMerchantIdAsyncMethod(),
            io.grpc.stub.ServerCalls.asyncBidiStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Merchant,
                com.paytm.pg.Merchant>(
                  this, METHODID_FIND_BY_MERCHANT_ID_ASYNC)))
          .addMethod(
            getFindByContractIdAsyncMethod(),
            io.grpc.stub.ServerCalls.asyncClientStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Merchant,
                com.paytm.pg.Merchant>(
                  this, METHODID_FIND_BY_CONTRACT_ID_ASYNC)))
          .build();
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class MerchantServiceStub extends io.grpc.stub.AbstractAsyncStub<MerchantServiceStub> {
    private MerchantServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MerchantServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MerchantServiceStub(channel, callOptions);
    }

    /**
     */
    public void saveMerchant(com.paytm.pg.Merchant request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSaveMerchantMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findByMerchantId(com.paytm.pg.Merchant request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindByMerchantIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void findByContractId(com.paytm.pg.Merchant request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getFindByContractIdMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> findByMerchantIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncBidiStreamingCall(
          getChannel().newCall(getFindByMerchantIdAsyncMethod(), getCallOptions()), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> findByContractIdAsync(
        io.grpc.stub.StreamObserver<com.paytm.pg.Merchant> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncClientStreamingCall(
          getChannel().newCall(getFindByContractIdAsyncMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class MerchantServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<MerchantServiceBlockingStub> {
    private MerchantServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MerchantServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MerchantServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.paytm.pg.Merchant saveMerchant(com.paytm.pg.Merchant request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSaveMerchantMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.paytm.pg.Merchant findByMerchantId(com.paytm.pg.Merchant request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindByMerchantIdMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.paytm.pg.Merchant findByContractId(com.paytm.pg.Merchant request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getFindByContractIdMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The Employee Service definition.
   * </pre>
   */
  public static final class MerchantServiceFutureStub extends io.grpc.stub.AbstractFutureStub<MerchantServiceFutureStub> {
    private MerchantServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MerchantServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MerchantServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Merchant> saveMerchant(
        com.paytm.pg.Merchant request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSaveMerchantMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Merchant> findByMerchantId(
        com.paytm.pg.Merchant request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindByMerchantIdMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.paytm.pg.Merchant> findByContractId(
        com.paytm.pg.Merchant request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getFindByContractIdMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SAVE_MERCHANT = 0;
  private static final int METHODID_FIND_BY_MERCHANT_ID = 1;
  private static final int METHODID_FIND_BY_CONTRACT_ID = 2;
  private static final int METHODID_FIND_BY_MERCHANT_ID_ASYNC = 3;
  private static final int METHODID_FIND_BY_CONTRACT_ID_ASYNC = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MerchantServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MerchantServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SAVE_MERCHANT:
          serviceImpl.saveMerchant((com.paytm.pg.Merchant) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Merchant>) responseObserver);
          break;
        case METHODID_FIND_BY_MERCHANT_ID:
          serviceImpl.findByMerchantId((com.paytm.pg.Merchant) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Merchant>) responseObserver);
          break;
        case METHODID_FIND_BY_CONTRACT_ID:
          serviceImpl.findByContractId((com.paytm.pg.Merchant) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Merchant>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FIND_BY_MERCHANT_ID_ASYNC:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.findByMerchantIdAsync(
              (io.grpc.stub.StreamObserver<com.paytm.pg.Merchant>) responseObserver);
        case METHODID_FIND_BY_CONTRACT_ID_ASYNC:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.findByContractIdAsync(
              (io.grpc.stub.StreamObserver<com.paytm.pg.Merchant>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MerchantServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MerchantServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.paytm.pg.MerchantOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MerchantService");
    }
  }

  private static final class MerchantServiceFileDescriptorSupplier
      extends MerchantServiceBaseDescriptorSupplier {
    MerchantServiceFileDescriptorSupplier() {}
  }

  private static final class MerchantServiceMethodDescriptorSupplier
      extends MerchantServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MerchantServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MerchantServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MerchantServiceFileDescriptorSupplier())
              .addMethod(getSaveMerchantMethod())
              .addMethod(getFindByMerchantIdMethod())
              .addMethod(getFindByContractIdMethod())
              .addMethod(getFindByMerchantIdAsyncMethod())
              .addMethod(getFindByContractIdAsyncMethod())
              .build();
        }
      }
    }
    return result;
  }
}
