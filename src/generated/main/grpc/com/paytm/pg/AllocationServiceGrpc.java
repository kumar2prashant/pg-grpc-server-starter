package com.paytm.pg;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * The Allocation Service Definition
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: Allocation.proto")
public final class AllocationServiceGrpc {

  private AllocationServiceGrpc() {}

  public static final String SERVICE_NAME = "com.paytm.pg.AllocationService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.paytm.pg.Allocation,
      com.paytm.pg.Allocation> getGetAllocationByEmployeeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllocationByEmployee",
      requestType = com.paytm.pg.Allocation.class,
      responseType = com.paytm.pg.Allocation.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.paytm.pg.Allocation,
      com.paytm.pg.Allocation> getGetAllocationByEmployeeMethod() {
    io.grpc.MethodDescriptor<com.paytm.pg.Allocation, com.paytm.pg.Allocation> getGetAllocationByEmployeeMethod;
    if ((getGetAllocationByEmployeeMethod = AllocationServiceGrpc.getGetAllocationByEmployeeMethod) == null) {
      synchronized (AllocationServiceGrpc.class) {
        if ((getGetAllocationByEmployeeMethod = AllocationServiceGrpc.getGetAllocationByEmployeeMethod) == null) {
          AllocationServiceGrpc.getGetAllocationByEmployeeMethod = getGetAllocationByEmployeeMethod =
              io.grpc.MethodDescriptor.<com.paytm.pg.Allocation, com.paytm.pg.Allocation>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getAllocationByEmployee"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Allocation.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.paytm.pg.Allocation.getDefaultInstance()))
              .setSchemaDescriptor(new AllocationServiceMethodDescriptorSupplier("getAllocationByEmployee"))
              .build();
        }
      }
    }
    return getGetAllocationByEmployeeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AllocationServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AllocationServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AllocationServiceStub>() {
        @java.lang.Override
        public AllocationServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AllocationServiceStub(channel, callOptions);
        }
      };
    return AllocationServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AllocationServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AllocationServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AllocationServiceBlockingStub>() {
        @java.lang.Override
        public AllocationServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AllocationServiceBlockingStub(channel, callOptions);
        }
      };
    return AllocationServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AllocationServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AllocationServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AllocationServiceFutureStub>() {
        @java.lang.Override
        public AllocationServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AllocationServiceFutureStub(channel, callOptions);
        }
      };
    return AllocationServiceFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * The Allocation Service Definition
   * </pre>
   */
  public static abstract class AllocationServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Get Allocation using Employee ID
     * IDL of server streaming RPCs where the client sends a request to the server and gets a stream to read a sequence of messages back. The client reads from the returned stream until there are no more messages. gRPC guarantees message ordering within an individual RPC call.
     * </pre>
     */
    public void getAllocationByEmployee(com.paytm.pg.Allocation request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Allocation> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetAllocationByEmployeeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetAllocationByEmployeeMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                com.paytm.pg.Allocation,
                com.paytm.pg.Allocation>(
                  this, METHODID_GET_ALLOCATION_BY_EMPLOYEE)))
          .build();
    }
  }

  /**
   * <pre>
   * The Allocation Service Definition
   * </pre>
   */
  public static final class AllocationServiceStub extends io.grpc.stub.AbstractAsyncStub<AllocationServiceStub> {
    private AllocationServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AllocationServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AllocationServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * Get Allocation using Employee ID
     * IDL of server streaming RPCs where the client sends a request to the server and gets a stream to read a sequence of messages back. The client reads from the returned stream until there are no more messages. gRPC guarantees message ordering within an individual RPC call.
     * </pre>
     */
    public void getAllocationByEmployee(com.paytm.pg.Allocation request,
        io.grpc.stub.StreamObserver<com.paytm.pg.Allocation> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetAllocationByEmployeeMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The Allocation Service Definition
   * </pre>
   */
  public static final class AllocationServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<AllocationServiceBlockingStub> {
    private AllocationServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AllocationServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AllocationServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Get Allocation using Employee ID
     * IDL of server streaming RPCs where the client sends a request to the server and gets a stream to read a sequence of messages back. The client reads from the returned stream until there are no more messages. gRPC guarantees message ordering within an individual RPC call.
     * </pre>
     */
    public java.util.Iterator<com.paytm.pg.Allocation> getAllocationByEmployee(
        com.paytm.pg.Allocation request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetAllocationByEmployeeMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The Allocation Service Definition
   * </pre>
   */
  public static final class AllocationServiceFutureStub extends io.grpc.stub.AbstractFutureStub<AllocationServiceFutureStub> {
    private AllocationServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AllocationServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AllocationServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_GET_ALLOCATION_BY_EMPLOYEE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AllocationServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AllocationServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALLOCATION_BY_EMPLOYEE:
          serviceImpl.getAllocationByEmployee((com.paytm.pg.Allocation) request,
              (io.grpc.stub.StreamObserver<com.paytm.pg.Allocation>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AllocationServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AllocationServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.paytm.pg.AllocationOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AllocationService");
    }
  }

  private static final class AllocationServiceFileDescriptorSupplier
      extends AllocationServiceBaseDescriptorSupplier {
    AllocationServiceFileDescriptorSupplier() {}
  }

  private static final class AllocationServiceMethodDescriptorSupplier
      extends AllocationServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AllocationServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AllocationServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AllocationServiceFileDescriptorSupplier())
              .addMethod(getGetAllocationByEmployeeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
