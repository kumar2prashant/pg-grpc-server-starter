// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: route_guide.proto

package io.grpc.examples.routeguide;

public interface RectangleOrBuilder extends
    // @@protoc_insertion_point(interface_extends:routeguide.Rectangle)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.routeguide.Point lo = 1;</code>
   * @return Whether the lo field is set.
   */
  boolean hasLo();
  /**
   * <code>.routeguide.Point lo = 1;</code>
   * @return The lo.
   */
  io.grpc.examples.routeguide.Point getLo();
  /**
   * <code>.routeguide.Point lo = 1;</code>
   */
  io.grpc.examples.routeguide.PointOrBuilder getLoOrBuilder();

  /**
   * <code>.routeguide.Point hi = 2;</code>
   * @return Whether the hi field is set.
   */
  boolean hasHi();
  /**
   * <code>.routeguide.Point hi = 2;</code>
   * @return The hi.
   */
  io.grpc.examples.routeguide.Point getHi();
  /**
   * <code>.routeguide.Point hi = 2;</code>
   */
  io.grpc.examples.routeguide.PointOrBuilder getHiOrBuilder();
}
