// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Merchant.proto

package com.paytm.pg;

/**
 * Protobuf type {@code com.paytm.pg.MerchantGateways}
 */
public final class MerchantGateways extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:com.paytm.pg.MerchantGateways)
    MerchantGatewaysOrBuilder {
private static final long serialVersionUID = 0L;
  // Use MerchantGateways.newBuilder() to construct.
  private MerchantGateways(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private MerchantGateways() {
    gatewayName_ = "";
    gatewayId_ = "";
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new MerchantGateways();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private MerchantGateways(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            java.lang.String s = input.readStringRequireUtf8();

            gatewayName_ = s;
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            gatewayId_ = s;
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.paytm.pg.MerchantOuterClass.internal_static_com_paytm_pg_MerchantGateways_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.paytm.pg.MerchantOuterClass.internal_static_com_paytm_pg_MerchantGateways_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.paytm.pg.MerchantGateways.class, com.paytm.pg.MerchantGateways.Builder.class);
  }

  public static final int GATEWAYNAME_FIELD_NUMBER = 1;
  private volatile java.lang.Object gatewayName_;
  /**
   * <code>string gatewayName = 1;</code>
   * @return The gatewayName.
   */
  @java.lang.Override
  public java.lang.String getGatewayName() {
    java.lang.Object ref = gatewayName_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      gatewayName_ = s;
      return s;
    }
  }
  /**
   * <code>string gatewayName = 1;</code>
   * @return The bytes for gatewayName.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getGatewayNameBytes() {
    java.lang.Object ref = gatewayName_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      gatewayName_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int GATEWAYID_FIELD_NUMBER = 2;
  private volatile java.lang.Object gatewayId_;
  /**
   * <code>string gatewayId = 2;</code>
   * @return The gatewayId.
   */
  @java.lang.Override
  public java.lang.String getGatewayId() {
    java.lang.Object ref = gatewayId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      gatewayId_ = s;
      return s;
    }
  }
  /**
   * <code>string gatewayId = 2;</code>
   * @return The bytes for gatewayId.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getGatewayIdBytes() {
    java.lang.Object ref = gatewayId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      gatewayId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getGatewayNameBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 1, gatewayName_);
    }
    if (!getGatewayIdBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, gatewayId_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getGatewayNameBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, gatewayName_);
    }
    if (!getGatewayIdBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, gatewayId_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.paytm.pg.MerchantGateways)) {
      return super.equals(obj);
    }
    com.paytm.pg.MerchantGateways other = (com.paytm.pg.MerchantGateways) obj;

    if (!getGatewayName()
        .equals(other.getGatewayName())) return false;
    if (!getGatewayId()
        .equals(other.getGatewayId())) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + GATEWAYNAME_FIELD_NUMBER;
    hash = (53 * hash) + getGatewayName().hashCode();
    hash = (37 * hash) + GATEWAYID_FIELD_NUMBER;
    hash = (53 * hash) + getGatewayId().hashCode();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.paytm.pg.MerchantGateways parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.paytm.pg.MerchantGateways parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.paytm.pg.MerchantGateways parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.paytm.pg.MerchantGateways parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.paytm.pg.MerchantGateways prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.paytm.pg.MerchantGateways}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.paytm.pg.MerchantGateways)
      com.paytm.pg.MerchantGatewaysOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.paytm.pg.MerchantOuterClass.internal_static_com_paytm_pg_MerchantGateways_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.paytm.pg.MerchantOuterClass.internal_static_com_paytm_pg_MerchantGateways_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.paytm.pg.MerchantGateways.class, com.paytm.pg.MerchantGateways.Builder.class);
    }

    // Construct using com.paytm.pg.MerchantGateways.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      gatewayName_ = "";

      gatewayId_ = "";

      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.paytm.pg.MerchantOuterClass.internal_static_com_paytm_pg_MerchantGateways_descriptor;
    }

    @java.lang.Override
    public com.paytm.pg.MerchantGateways getDefaultInstanceForType() {
      return com.paytm.pg.MerchantGateways.getDefaultInstance();
    }

    @java.lang.Override
    public com.paytm.pg.MerchantGateways build() {
      com.paytm.pg.MerchantGateways result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public com.paytm.pg.MerchantGateways buildPartial() {
      com.paytm.pg.MerchantGateways result = new com.paytm.pg.MerchantGateways(this);
      result.gatewayName_ = gatewayName_;
      result.gatewayId_ = gatewayId_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.paytm.pg.MerchantGateways) {
        return mergeFrom((com.paytm.pg.MerchantGateways)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.paytm.pg.MerchantGateways other) {
      if (other == com.paytm.pg.MerchantGateways.getDefaultInstance()) return this;
      if (!other.getGatewayName().isEmpty()) {
        gatewayName_ = other.gatewayName_;
        onChanged();
      }
      if (!other.getGatewayId().isEmpty()) {
        gatewayId_ = other.gatewayId_;
        onChanged();
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.paytm.pg.MerchantGateways parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.paytm.pg.MerchantGateways) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private java.lang.Object gatewayName_ = "";
    /**
     * <code>string gatewayName = 1;</code>
     * @return The gatewayName.
     */
    public java.lang.String getGatewayName() {
      java.lang.Object ref = gatewayName_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        gatewayName_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string gatewayName = 1;</code>
     * @return The bytes for gatewayName.
     */
    public com.google.protobuf.ByteString
        getGatewayNameBytes() {
      java.lang.Object ref = gatewayName_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        gatewayName_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string gatewayName = 1;</code>
     * @param value The gatewayName to set.
     * @return This builder for chaining.
     */
    public Builder setGatewayName(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      gatewayName_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string gatewayName = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearGatewayName() {
      
      gatewayName_ = getDefaultInstance().getGatewayName();
      onChanged();
      return this;
    }
    /**
     * <code>string gatewayName = 1;</code>
     * @param value The bytes for gatewayName to set.
     * @return This builder for chaining.
     */
    public Builder setGatewayNameBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      gatewayName_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object gatewayId_ = "";
    /**
     * <code>string gatewayId = 2;</code>
     * @return The gatewayId.
     */
    public java.lang.String getGatewayId() {
      java.lang.Object ref = gatewayId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        gatewayId_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string gatewayId = 2;</code>
     * @return The bytes for gatewayId.
     */
    public com.google.protobuf.ByteString
        getGatewayIdBytes() {
      java.lang.Object ref = gatewayId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        gatewayId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string gatewayId = 2;</code>
     * @param value The gatewayId to set.
     * @return This builder for chaining.
     */
    public Builder setGatewayId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      gatewayId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string gatewayId = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearGatewayId() {
      
      gatewayId_ = getDefaultInstance().getGatewayId();
      onChanged();
      return this;
    }
    /**
     * <code>string gatewayId = 2;</code>
     * @param value The bytes for gatewayId to set.
     * @return This builder for chaining.
     */
    public Builder setGatewayIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      gatewayId_ = value;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:com.paytm.pg.MerchantGateways)
  }

  // @@protoc_insertion_point(class_scope:com.paytm.pg.MerchantGateways)
  private static final com.paytm.pg.MerchantGateways DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.paytm.pg.MerchantGateways();
  }

  public static com.paytm.pg.MerchantGateways getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<MerchantGateways>
      PARSER = new com.google.protobuf.AbstractParser<MerchantGateways>() {
    @java.lang.Override
    public MerchantGateways parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new MerchantGateways(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<MerchantGateways> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<MerchantGateways> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public com.paytm.pg.MerchantGateways getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

